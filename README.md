# Hindawi Peer Review Platform
[Review](review.hindawi.com) is a system that allows authors to submit manuscripts and editors to manage the peer review for a particular journal on Hindawi.
For more details about Hindawi, please visit: https://www.hindawi.com/

## Structure

This repository is a monorepo containing Pubsweet components used and created by Hindawi.
The app entry point is `packages/app`, the place where the `xpub-review` setup and configurations are stored.

## Installing

In the root directory, run `yarn` to install all the dependencies. 


## Configuration
Add the following values to `packages/app/config/local-development.json`

```json
{
  "pubsweet-server": {
    "secret": "__EDIT_THIS__"
  }
}
```

xPub-review is using external services as AWS, MTS-FTP, Publons, ORCID. In order to run the app locally a `.env` file is mandatory with keys and settings for each service.

Contact us at technology@hindawi.com for help getting setup.

## Running the app

1. Open Docker engine
2. Start services with `yarn start:services`
3. The first time you run the app, initialize the database with `yarn run setupdb` (press Enter when asked for a collection title, to skip that step).
4. In case of migrations needed follow `packages/app` and run  `npx pubsweet migrate` or `./node_modules/.bin/pubsweet migrate`
5. Start app with `yarn start` in root


## Community

Join [the Mattermost channel](https://mattermost.coko.foundation/coko/channels/xpub) for discussion of xpub.


## Migrations

1. use folder `./packages/app/migrations`
2. add file named `${number}-${name}(.sql/.js)` e.g. `001-user-add-name-field.sql`



