const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')

const resolvers = {
  Query: {
    async getCurrentUser(_, { input }, ctx) {
      return useCases.getCurrentUserUseCase
        .initialize(models)
        .execute({ userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
