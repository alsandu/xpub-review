const config = require('config')
const { get, flatten } = require('lodash')

const activityLogObjectTypes = config.get('activityLogObjectTypes')

const initialize = ({
  Manuscript,
  AuditLog,
  Identity,
  Team,
  TeamMember,
  User,
}) => ({
  async execute(manuscriptId) {
    const manuscript = await Manuscript.findOneByField('id', manuscriptId)
    const manuscripts = await Manuscript.findBy(
      { submissionId: manuscript.submissionId },
      'logs.user.[identities, teamMemberships.[team]]',
    )

    let auditLogs = flatten(
      manuscripts.map(m => m.logs.map(log => ({ ...log, version: m.version }))),
    )

    auditLogs = await Promise.all(
      auditLogs.map(async log => {
        const logObjectType = get(activityLogObjectTypes, log.objectType)
        let userIdentity, userRole, userNumber
        if (log.user) {
          userIdentity = log.user.identities.find(i => i.type === 'local')
          userRole = log.user.teamMemberships[0].team.role
          userNumber = log.user.teamMemberships[0].reviewerNumber
        }
        let objectIdentity, objectRole
        if (logObjectType === 'user') {
          const object = await User.findOneBy({
            queryObject: {
              id: log.objectId,
            },
            eagerLoadRelations: ['identities', 'teamMemberships.[team]'],
          })
          objectIdentity = object.identities.find(i => i.type === 'local')
          objectRole = get(object, 'teamMemberships[0].team.role', '')
        }
        const activityLogEvents = config.get('activityLogEvents')
        return {
          id: log.id,
          user: {
            role: userRole,
            email: get(userIdentity, 'email'),
            reviewerNumber: userNumber,
          },
          action: get(activityLogEvents, log.action),
          target: {
            role: objectRole,
            email: get(objectIdentity, 'email'),
            reviewerNumber: userNumber,
          },
          created: log.created,
          version: log.version,
        }
      }),
    )

    auditLogs = auditLogs.reduce((acc, cur) => {
      if (!acc.find(obj => obj.version === cur.version)) {
        acc.push({ version: cur.version, logs: [] })
      }
      const logGroup = acc.find(obj => obj.version === cur.version)
      logGroup.logs.push(cur)
      return acc
    }, [])

    return auditLogs
  },
})

const authsomePolicies = ['authenticatedUser', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
