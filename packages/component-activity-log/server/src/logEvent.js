const config = require('config')
const AuditLog = require('component-model-audit-log').model
const Manuscript = require('component-model-manuscript').model

const logEvent = async logObj => {
  const { manuscriptId, journalId } = logObj
  if (!journalId) {
    const manuscript = await Manuscript.find(manuscriptId)
    logObj.journalId = manuscript.journalId
  }
  const log = new AuditLog(logObj)
  await log.save()
}

logEvent.actions = Object.keys(config.get('activityLogEvents')).reduce(
  (acc, key) => ({
    ...acc,
    [key]: key,
  }),
  {},
)

logEvent.objectType = Object.keys(config.get('activityLogObjectTypes')).reduce(
  (acc, key) => ({
    ...acc,
    [key]: key,
  }),
  {},
)

module.exports = {
  logEvent,
}
