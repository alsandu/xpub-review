const TeamMember = require('./src/teamMember')
const resolvers = require('./src/resolvers')

module.exports = {
  model: TeamMember,
  modelName: 'TeamMember',
  resolvers,
}
