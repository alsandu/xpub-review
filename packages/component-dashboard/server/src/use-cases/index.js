const getForManuscriptRolesUseCase = require('./getForManuscriptRoles')
const getForGlobalRolesUseCase = require('./getForGlobalRoles')
const getManuscriptsUseCase = require('./getManuscripts')

const deleteManuscriptUseCase = require('./deleteManuscript')
const archiveManuscriptUseCase = require('./archiveManuscript')

module.exports = {
  getForManuscriptRolesUseCase,
  getForGlobalRolesUseCase,
  getManuscriptsUseCase,
  deleteManuscriptUseCase,
  archiveManuscriptUseCase,
}
