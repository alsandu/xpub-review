module.exports = {
  addAuthorToManuscript: async ({ manuscript, fixtures, user }) => {
    const team = fixtures.generateTeam({
      manuscriptId: manuscript.id,
      role: 'author',
    })

    team.manuscript = manuscript
    manuscript.teams = [team]

    const author = user || fixtures.generateUser({})

    const teamMember = fixtures.generateTeamMember({
      userId: author.id,
      teamId: team.id,
    })

    author.teamMemberships = author.teamMemberships || []
    author.teamMemberships.push(teamMember)

    await teamMember.linkUser(author)

    teamMember.team = team
    team.members.push(teamMember)

    return author
  },
}
