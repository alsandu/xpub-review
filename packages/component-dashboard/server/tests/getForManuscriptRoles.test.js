process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const { models, fixtures } = require('fixture-service')

const { getForManuscriptRolesUseCase } = require('../src/use-cases')

const { addAuthorToManuscript } = require('./helpers')

describe('get manusripts for manuscript roles (HE, author, reviewer)', () => {
  it('returns own manuscripts if the user is an author', async () => {
    const manuscript = fixtures.generateManuscript({
      status: 'technicalChecks',
      customId: '1112223',
    })
    fixtures.generateManuscript({})

    const user = await addAuthorToManuscript({ manuscript, fixtures })

    const draftManuscript = fixtures.generateManuscript({
      status: 'draft',
    })

    await addAuthorToManuscript({
      manuscript: draftManuscript,
      fixtures,
      user,
    })

    const submittedManuscript = fixtures.generateManuscript({
      status: 'submitted',
      customId: '1112224',
    })

    await addAuthorToManuscript({
      manuscript: submittedManuscript,
      fixtures,
      user,
    })

    const mockedModels = models.build(fixtures)

    const manuscripts = await getForManuscriptRolesUseCase
      .initialize(mockedModels)
      .execute({ input: { dateOrder: 'desc' }, user })

    expect(manuscripts).toHaveLength(user.teamMemberships.length)
    expect(manuscripts[0].role).toEqual('author')
    expect(manuscripts[0].authors).toHaveLength(1)

    expect(manuscripts[0].customId).toEqual(submittedManuscript.customId)
    expect(manuscripts[0].visibleStatus).toEqual('Submitted')

    expect(manuscripts[1].visibleStatus).toEqual('Complete Submission')

    expect(manuscripts[2].customId).toBeUndefined()
  })
})
