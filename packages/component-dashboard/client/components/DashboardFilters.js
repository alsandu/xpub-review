import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Item, Text, Label, Menu } from 'component-hindawi-ui'

const DashboardFilters = ({
  values,
  options,
  changeSort,
  isConfirmed,
  changePriority,
}) => (
  <Root isConfirmed={isConfirmed}>
    <Row alignItems="flex-end" justify="flex-start" pt={isConfirmed ? 11 : 14}>
      <Text mr={1} pb={1} secondary>
        Filters
      </Text>
      <Item
        alignItems="flex-start"
        data-test-id="dashboard-filter-priority"
        flex={0}
        mr={1}
        vertical
      >
        <Label>Priority</Label>
        <Menu
          inline
          onChange={changePriority}
          options={options.priority}
          placeholder="Please select"
          value={values.priority}
          width={14}
        />
      </Item>
      <Item
        alignItems="flex-start"
        data-test-id="dashboard-filter-order"
        flex={0}
        vertical
      >
        <Label>Order</Label>
        <Menu
          inline
          onChange={changeSort}
          options={options.sort}
          placeholder="Please select"
          value={values.sort}
        />
      </Item>
    </Row>
  </Root>
)

export default DashboardFilters

const Root = styled.div`
  /* The filter slot's height goes to the top of
  the page because of Safari's implementation
  (weird element jumping when scrolling) */
  height: calc(
    ${({ isConfirmed }) => (isConfirmed ? 18 : 21)} * ${th('gridUnit')}
  );
  position: fixed;
  top: 0;
  left: calc(${th('gridUnit')} * 10);
  right: calc(${th('gridUnit')} * 10);
  background-color: ${th('colorBackground')};
`
