import { compose, withProps, withState, withHandlers } from 'recompose'

export const PRIORITY_VALUES = {
  ALL: 'all',
  NEEDS_ATTENTION: 'needsAttention',
  IN_PROGRESS: 'inProgress',
  ARCHIVED: 'archived',
}
const priorityOptions = [
  { label: 'All', value: PRIORITY_VALUES.ALL },
  { label: 'Needs Attention', value: PRIORITY_VALUES.NEEDS_ATTENTION },
  { label: 'In Progress', value: PRIORITY_VALUES.IN_PROGRESS },
  { label: 'Archived', value: PRIORITY_VALUES.ARCHIVED },
]

export const SORT_VALUES = {
  ASC: 'asc',
  DESC: 'desc',
}
const sortOptions = [
  { label: 'Oldest first', value: SORT_VALUES.ASC },
  { label: 'Newest first', value: SORT_VALUES.DESC },
]

const defaultFilterValues = {
  priority: PRIORITY_VALUES.ALL,
  sort: SORT_VALUES.DESC,
}

const hydrateFilters = defaultValues => {
  const localFilterValues = localStorage.getItem('filters')

  if (localFilterValues) return JSON.parse(localFilterValues)
  return defaultValues
}

export default compose(
  withProps(() => ({
    options: {
      sort: sortOptions,
      priority: priorityOptions,
    },
  })),
  withState('values', 'setFilterValues', hydrateFilters(defaultFilterValues)),
  withHandlers({
    changeFilterValue: ({ values, setFilterValues }) => type => value => {
      const newValues = {
        ...values,
        [type]: value,
      }
      setFilterValues(newValues)
      localStorage.setItem('filters', JSON.stringify(newValues))
    },
  }),
  withProps(({ values, changeFilterValue, options }) => ({
    filters: {
      values,
      options,
      changeSort: changeFilterValue('sort'),
      changePriority: changeFilterValue('priority'),
    },
  })),
)
