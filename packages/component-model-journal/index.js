const fs = require('fs')
const path = require('path')

const Journal = require('./src/entities/journal')
const resolvers = require('./src/resolvers')

module.exports = {
  model: Journal,
  modelName: 'Journal',
  resolvers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/src/typeDefs.graphqls'),
    'utf8',
  ),
}
