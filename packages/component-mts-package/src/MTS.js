const { get } = require('lodash')

const PackageManager = require('./PackageManager')

const config = require('config')

const defaultS3Config = config.get('pubsweet-component-aws-s3')
const MTSConfig = config.get('mts-service')

const { convertToXML, composeJson } = require('./helpers')

module.exports = {
  async sendPackage({
    manuscript = {},
    isEQA = false,
    config = MTSConfig.journal,
    s3Config = defaultS3Config,
    ftpConfig = MTSConfig.ftp,
    options = MTSConfig.xml,
  }) {
    const composedJson = composeJson({
      isEQA,
      config,
      options,
      manuscript,
    })
    const xmlFile = convertToXML({
      options,
      json: composedJson,
      prefix: config.prefix,
    })

    return PackageManager.createFilesPackage(s3Config)({
      manuscript,
      xmlFile,
      isEQA,
    }).then(() => {
      const packageName = get(xmlFile, 'name', '').replace('.xml', '')
      const filename = isEQA
        ? `ACCEPTED_${packageName}.${manuscript.version}.zip`
        : `${packageName}.zip`

      return PackageManager.uploadFiles({
        filename,
        s3Config,
        config: ftpConfig,
      })
    })
  },
}
