const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')
const services = require('./services')

const resolvers = {
  Query: {
    async loadReviewerSuggestions(_, { manuscriptId }, ctx) {
      return useCases.loadReviewerSuggestionsUseCase
        .initialize({
          models,
          reviewerSuggestionsService: services.publonsService,
        })
        .execute(manuscriptId)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
