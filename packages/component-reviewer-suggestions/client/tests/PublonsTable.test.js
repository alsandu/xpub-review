import React from 'react'
import 'jest-styled-components'
import 'jest-dom/extend-expect'
import { waitForElement, fireEvent } from 'react-testing-library'
import PublonsTable from '../components/PublonsTable'
import { render } from './testUtils'

describe('Publons Table ', () => {
  const canInvitePublons = true
  it('Should render all reviews', async () => {
    const { getByTestId } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        manuscriptId="558131fe-b308-548b-8e6c-750c59380466"
      />,
    )
    const firstReviewer = await waitForElement(() =>
      getByTestId('name-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
    )
    const secondReviewer = await waitForElement(() =>
      getByTestId('name-9484df8e-e5c0-58d8-bc4e-0642409fc149'),
    )

    expect(firstReviewer.textContent).toEqual('Lora Brent')
    expect(
      getByTestId('aff-9484df8e-e5c0-58d8-bc4e-0642409fc349').textContent,
    ).toEqual('Alaska Air Group, Inc.')
    expect(
      getByTestId('numberOfReviews-9484df8e-e5c0-58d8-bc4e-0642409fc349')
        .textContent,
    ).toEqual('34')
    expect(
      getByTestId('reviewerPublonsButton-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
    ).toBeInTheDocument()

    expect(secondReviewer.textContent).toEqual('Charles Lottie')
    expect(
      getByTestId('aff-9484df8e-e5c0-58d8-bc4e-0642409fc149').textContent,
    ).toEqual('Corning Inc.')
    expect(
      getByTestId('numberOfReviews-9484df8e-e5c0-58d8-bc4e-0642409fc149')
        .textContent,
    ).toEqual('55')
    expect(
      getByTestId('reviewerPublonsButton-9484df8e-e5c0-58d8-bc4e-0642409fc149'),
    ).toBeInTheDocument()
  })

  it('Should open modal after inviting one reviewer', async () => {
    const { getByTestId, getByText } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        manuscriptId="558131fe-b308-548b-8e6c-750c59380466"
      />,
    )

    await waitForElement(() =>
      getByTestId('name-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
    )

    expect(
      getByTestId('reviewerPublonsButton-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
    ).toBeInTheDocument()

    fireEvent.click(
      getByTestId('reviewerPublonsButton-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
    )
    expect(getByText('Send Invitation to Review?')).toBeInTheDocument()
    expect(getByText('SEND')).toBeInTheDocument()
    expect(getByText('BACK')).toBeInTheDocument()
  })

  it('Should return error if there are no reviewers', async () => {
    const { getByTestId } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        manuscriptId="6638-2324"
      />,
    )

    await waitForElement(() => getByTestId('error-empty-state'))

    expect(getByTestId('error-empty-state')).toBeInTheDocument()
    expect(getByTestId('error-empty-state')).toHaveTextContent(
      'There are no reviewer suggestions to display',
    )
  })
})
