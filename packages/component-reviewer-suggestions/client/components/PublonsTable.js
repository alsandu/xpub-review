import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { get, isEmpty } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Spinner } from '@pubsweet/ui'
import { withModal } from 'component-modal'
import { compose, withHandlers } from 'recompose'
import { Query } from 'react-apollo'
import {
  Row,
  Tag,
  Item,
  Text,
  Label,
  ActionLink,
  MultiAction,
} from 'component-hindawi-ui'

import { loadReviewerSuggestions } from '../graphql/queries'

const PublonsTable = ({ onInvite, manuscriptId, canInvitePublons }) => (
  <Wrapper>
    <Query query={loadReviewerSuggestions} variables={{ manuscriptId }}>
      {({ data: { loadReviewerSuggestions: reviewers }, loading }) => {
        if (loading) {
          return (
            <Row mt={3}>
              <Spinner />
            </Row>
          )
        }
        if (isEmpty(reviewers)) {
          return (
            <Row mb={1} mt={3}>
              <Item alignItems="center" justify="center">
                <Text data-test-id="error-empty-state" emptyState>
                  There are no reviewer suggestions to display
                </Text>
              </Item>
            </Row>
          )
        }
        return (
          <TableView
            canInvitePublons={canInvitePublons}
            onInvite={onInvite}
            reviewers={reviewers}
          />
        )
      }}
    </Query>
  </Wrapper>
)

const TableView = ({ canInvitePublons, reviewers, onInvite }) => (
  <Table>
    <thead>
      <tr>
        <th>
          <Label>Full Name</Label>
        </th>
        <th>
          <Label>Affiliation</Label>
        </th>
        <th>
          <Label>No. of Reviews</Label>
        </th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      {reviewers.map(reviewer => (
        <TableRow disabled={!!reviewer.isInvited} key={reviewer.id}>
          <td data-test-id={`name-${reviewer.id}`}>
            <ActionLink
              opacity={reviewer.isInvited ? 0.3 : 1}
              to={get(reviewer, 'profileUrl', 'https://www.publons.com/')}
            >
              {`${get(reviewer, 'givenNames', '')} ${get(
                reviewer,
                'surname',
                '',
              )}`}
            </ActionLink>
          </td>
          <td>
            <Text
              data-test-id={`aff-${reviewer.id}`}
              display="inline-flex"
              opacity={reviewer.isInvited ? 0.3 : 1}
            >{`${get(reviewer, 'aff', '')}`}</Text>
          </td>
          <td>
            <Text
              data-test-id={`numberOfReviews-${reviewer.id}`}
              display="inline-flex"
              opacity={reviewer.isInvited ? 0.3 : 1}
            >{`${get(reviewer, 'numberOfReviews', '')}`}</Text>
          </td>
          {reviewer.isInvited ? (
            <TagCell>
              <Tag mr={1} pending>
                INVITED
              </Tag>
            </TagCell>
          ) : (
            <HiddenCell>
              {canInvitePublons ? (
                <Button
                  data-test-id={`reviewerPublonsButton-${reviewer.id}`}
                  mr={1}
                  onClick={onInvite(reviewer)}
                  primary
                  size="small"
                >
                  SEND
                </Button>
              ) : (
                <Button
                  data-test-id={`reviewerPublonsButton-disabled-${reviewer.id}`}
                  disabled
                  mr={1}
                  size="small"
                >
                  SEND
                </Button>
              )}
            </HiddenCell>
          )}
        </TableRow>
      ))}
    </tbody>
  </Table>
)
export default compose(
  withModal({
    modalKey: 'invite-reviewer',
    component: MultiAction,
  }),
  withHandlers({
    onInvite: ({ showModal, onInvite }) => reviewer => () => {
      const { aff, email, givenNames, surname } = reviewer
      showModal({
        title: 'Send Invitation to Review?',
        confirmText: 'SEND',
        cancelText: 'BACK',
        onConfirm: modalProps =>
          onInvite({ aff, email, givenNames, surname }, modalProps),
      })
    },
  }),
)(PublonsTable)

PublonsTable.propTypes = {
  /** Sends an invitation to the reviewer. */
  onInvite: PropTypes.func,
}

PublonsTable.defaultProps = {
  onInvite: () => {},
}

// #region styles
const Table = styled.table`
  border-collapse: collapse;
  width: 100%;

  & thead {
    border: 1px solid ${th('colorBorder')};
    background-color: ${th('colorBackgroundHue2')};
    padding-top: calc(${th('gridUnit')} * 2);
  }

  & th,
  & td {
    border: none;
    padding-left: calc(${th('gridUnit')} * 2);
    text-align: start;
    vertical-align: middle;

    height: calc(${th('gridUnit')} * 5);
    min-width: calc(${th('gridUnit')} * 12);
  }
`

const HiddenCell = styled.td`
  display: flex;
  justify-content: flex-end;
  opacity: 0;
  padding-top: ${th('gridUnit')};
`

const HidableCell = styled.td`
  opacity: 1;
  padding-top: ${th('gridUnit')};
`

const TagCell = styled.td`
  display: flex;
  justify-content: flex-end;
  padding-top: ${th('gridUnit')};
`
const TableCSS = props => {
  if (get(props, 'disabled', false)) {
    return css`
      background: ${th('colorBackgroundHue3')};
    `
  }
  return css`
    &:hover {
      background: ${th('colorBackgroundHue3')};
      ${HiddenCell} {
        opacity: 1;
      }
      ${HidableCell} {
        opacity: 0;
      }
    }
  `
}

const TableRow = styled.tr`
  border-bottom: 1px solid ${th('colorBorder')};
  & td:first-child {
    min-width: calc(${th('gridUnit')} * 20);
  }
  & td:last-child {
    vertical-align: top;
    text-align: right;
    padding-right: calc(${th('gridUnit')} * 2);
  }
  ${TableCSS}
`

const Wrapper = styled.div`
  display: block;
  height: calc(${th('gridUnit')} * 58);
  overflow-x: auto;
  background-color: ${th('colorBackgroundHue2')};
`
