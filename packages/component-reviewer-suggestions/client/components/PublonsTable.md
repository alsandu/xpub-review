A list of publon reviewers.

```js
const reviewers = [
  {
    id: '1',
    givenNames: 'Anca Ioana',
    surname: 'Ursachi',
    email: 'anca@thinslices.com',
    profileUrl: undefined,
    aff: 'Personality Team/ Feature Team/ Log Team/ Powerpuff Girls',
    numberOfReviews: 1067,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '2',
    givenNames: 'Tania',
    surname: 'Fecheta',
    email: 'tania@thinslices.com',
    profileUrl: undefined,
    aff: 'Personality Team/ Feature Team/ Log Team/ Powerpuff Girls',
    numberOfReviews: 865,
    type: 'publons',
    isInvited: true,
  },
  {
    id: '3',
    givenNames: 'Mihail',
    surname: 'Hagiu',
    email: 'mihaila@thinslices.com',
    profileUrl: undefined,
    aff: 'Personality Team/ Feature Team/ Log Team/ Powerpuff Girls',
    numberOfReviews: 566,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '4',
    givenNames: 'Alex',
    surname: 'Munteanu',
    email: 'alex@thinslices.com',
    profileUrl: undefined,
    aff: 'Funny Team',
    numberOfReviews: 454,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '5',
    givenNames: 'Sebi',
    surname: 'Mihalache',
    email: 'sebi@thinslices.com',
    profileUrl: undefined,
    aff: 'Funny Team',
    numberOfReviews: 83,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '6',
    givenNames: 'Luigi',
    surname: 'Mantu',
    email: 'lu@thinslices.com',
    profileUrl: undefined,
    aff: 'Design Team',
    numberOfReviews: 544,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '7',
    givenNames: 'Oana Elena',
    surname: 'Macsimiuc',
    email: 'eliza@thinslices.com',
    profileUrl: undefined,
    aff: 'Funny Team',
    numberOfReviews: 454,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '8',
    givenNames: 'Bogdan',
    surname: 'Cochior',
    email: 'bogdi@thinslices.com',
    profileUrl: undefined,
    aff: 'Boss Team',
    numberOfReviews: 345,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '9',
    givenNames: 'Alex',
    surname: 'Pricop',
    email: 'pi@thinslices.com',
    profileUrl: undefined,
    aff: 'Design Team',
    numberOfReviews: 44,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '10',
    givenNames: 'Larisa Andreea',
    surname: 'Andrici',
    email: 'bogdi@thinslices.com',
    profileUrl: undefined,
    aff: 'Smart Team',
    numberOfReviews: 655,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '11',
    givenNames: 'Adi',
    surname: 'Onofrei',
    email: 'adi@thinslices.com',
    profileUrl: undefined,
    aff: 'QA Team',
    numberOfReviews: 65,
    type: 'publons',
    isInvited: false,
  },
  {
    id: '12',
    givenNames: 'Sabina',
    surname: 'Deliu',
    email: 'sabina@thinslices.com',
    profileUrl: undefined,
    aff: 'QA Team',
    numberOfReviews: 48,
    type: 'publons',
    isInvited: false,
  },
]
;<ContextualBox startExpanded label="Reviewer Suggestions">
  <PublonsTable
    reviewers={reviewers}
    onInvite={(reviewer, modalProps) => {
      console.log('args:', { reviewer, modalProps })
    }}
  />
</ContextualBox>
```

There are no reviewers to display.

```js
const reviewers = []
;<ContextualBox startExpanded label="Reviewer Suggestions">
  <PublonsTable
    reviewers={reviewers}
    onInvite={(reviewer, modalProps) => {
      console.log('args:', { reviewer, modalProps })
    }}
  />
</ContextualBox>
```
