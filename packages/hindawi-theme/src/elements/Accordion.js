import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { heightHelper } from 'component-hindawi-ui'

const boxShadow = props => {
  if (props.transparent) {
    return css`
      box-shadow: transparent;
    `
  }

  if (props.highlight) {
    return css`
      border: 1px solid ${th('colorPrimary')};
      box-shadow: 0 0 2px 0 ${th('colorPrimary')};
    `
  }

  return css`
    box-shadow: ${th('boxShadow')};
  `
}

const bgColor = props => css`
  background-color: ${props =>
    props.transparent ? 'transparent' : th('accordion.headerBackgroundColor')};
`

export default {
  Root: css`
    border-radius: ${th('borderRadius')};
    flex: 1;

    ${bgColor};
    ${boxShadow};
  `,
  Header: {
    Root: css`
      border-radius: ${props => (props.expanded ? 0 : th('borderRadius'))};
      border-top-left-radius: ${th('borderRadius')};
      border-top-right-radius: ${th('borderRadius')};
      border-bottom: ${props =>
        props.expanded && !props.transparent ? th('accordion.border') : 'none'};

      ${bgColor};
      ${heightHelper};
    `,
    Label: css`
      color: ${th('colorText')};
      font-size: ${th('accordion.headerFontSize')};
      font-family: ${th('accordion.headerFontFamily')};
    `,
    Icon: css`
      color: ${th('colorText')};
    `,
  },
}
