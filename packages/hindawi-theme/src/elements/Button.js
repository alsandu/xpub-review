import { css } from 'styled-components'
import { lighten, th } from '@pubsweet/ui-toolkit'
import { marginHelper } from 'component-hindawi-ui'

const primary = css`
  background-color: ${th('button.primary')};
  border: ${th('borderStyle')} ${th('borderWidth')}
    ${th('button.borderDefault')};
  color: ${th('button.primaryText')};

  &:hover {
    background-color: ${lighten('button.primary', 0.1)};
  }

  &:focus,
  &:active {
    background-color: ${th('button.primary')};
    border-color: ${th('button.borderActive')};
    outline: none;
  }

  &[disabled] {
    border: none;
    &,
    &:hover {
      background-color: ${th('button.disabled')};
    }
  }
`

const secondary = css`
  background: none;
  border: ${th('borderStyle')} ${th('button.secondaryBorderWidth')}
    ${th('colorSecondary')};
  color: ${th('colorSecondary')};

  &:hover,
  &:focus,
  &:active {
    background: none;
    border-color: ${th('button.secondaryBorderColor')};
    outline: none;
  }

  &[disabled] {
    border-color: ${th('button.secondaryBorderColor')};
    color: ${th('colorTextPlaceholder')};
    opacity: 0.1;

    &:hover {
      background: none;
    }
  }
`

const buttonWidth = props => {
  if (props.width) {
    return css`
      width: calc(${th('gridUnit')} * ${props.width});
    `
  }

  return css`
    width: 100%;
  `
}

const buttonSize = props => {
  switch (props.size) {
    case 'small':
      return css`
        font-family: ${th('defaultFont')};
        font-size: ${th('button.smallSize')};
        font-weight: 600;
        height: calc(${th('gridUnit')} * 3);
        min-width: calc(${th('gridUnit')} * ${th('button.smallWidth')});
        width: calc(${th('gridUnit')} * ${th('button.smallWidth')});
      `
    case 'medium':
      return css`
        font-family: ${th('defaultFont')};
        font-size: ${th('button.mediumSize')};
        font-weight: 600;
        height: calc(${th('gridUnit')} * ${th('button.mediumHeight')});
        width: calc(${th('gridUnit')} * ${th('button.mediumWidth')});
      `
    case 'xLarge':
      return css`
        font-family: ${th('defaultFont')};
        font-size: ${th('button.xLargeSize')};
        font-weight: 600;
        height: calc(${th('gridUnit')} * ${th('button.xLargeHeight')});
        width: calc(${th('gridUnit')} * ${th('button.xLargeWidth')});
      `
    case 'large':
      return css`
        font-family: ${th('defaultFont')};
        font-size: ${th('button.largeSize')};
        font-weight: 600;
        height: calc(${th('gridUnit')} * ${th('button.largeHeight')});
        width: calc(${th('gridUnit')} * ${th('button.largeWidth')});
      `
    default:
      return css`
        font-family: ${th('defaultFont')};
        font-weight: 600;
        line-height: calc(${th('gridUnit')} * ${th('button.largeHeight')});
        height: calc(${th('gridUnit')} * ${th('button.largeHeight')});

        ${buttonWidth};
      `
  }
}

export default css`
  align-items: center;
  display: flex;
  justify-content: center;
  line-height: 1;
  padding: 0 calc(${th('gridUnit')});
  text-transform: uppercase;

  &:hover,
  &:focus {
    transition: none;
  }

  ${props => (props.primary ? primary : secondary)};
  ${buttonSize};
  ${marginHelper};
`
