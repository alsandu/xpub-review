import './fonts/index.css'

// all values are in gridUnits
// fonts and lineheights are the only exceptions
const colors = {
  actionPrimaryColor: '#63A945',
  actionSecondaryColor: '#007E92',
  backgroundColor: '#ECF0F3',
  contrastGrayColor: '#939393',
  furnitureColor: '#DBDBDB',
  infoColor: '#FCB74B',
  labelColor: '#003049',
  textPrimaryColor: '#242424',
  textSecondaryColor: '#586971',
  transparent: 'transparent',
  warningColor: '#FC6A4B',
  white: '#F6F6F6',
}

const sizes = {
  borderRadius: '5px',
  h1Size: '30px',
  h1LineHeight: '37px',
  h2Size: '24px',
  h2LineHeight: '29px',
  h3Size: '16px',
  h3LineHeight: '19px',
  h4Size: '14px',
  h4LineHeight: '18px',
  normalTextSize: '14px',
  normalTextLineHeight: '18px',
  secondaryTextSize: '14px',
  secondaryTextLineHeight: '17px',
}

const buttons = {
  small: {
    height: 3,
    fontSize: '12px',
    fontWeight: 700,
    lineHeight: '14px',
    minWidth: 10,
  },
  medium: {
    height: 4,
    fontSize: '13px',
    fontWeight: 700,
    lineHeight: '15px',
    minWidth: 12,
  },
  large: {
    height: 5,
    fontSize: '14px',
    fontWeight: 700,
    lineHeight: '18px',
    minWidth: 15,
  },
  xLarge: {
    height: 5,
    fontSize: '14px',
    fontWeight: 700,
    lineHeight: '18px',
    minWidth: 24,
  },
  primary: {
    backgroundColor: colors.actionPrimaryColor,
    color: colors.white,
    disabledBgColor: colors.furnitureColor,
    fontSize: sizes.h4Size,
    lineHeight: sizes.h4LineHeight,
  },
  secondary: {
    backgroundColor: colors.transparent,
    borderColor: colors.labelColor,
    color: colors.labelColor,
    disabledBorderColor: colors.furnitureColor,
    disabledColor: colors.furnitureColor,
    fontSize: sizes.h4Size,
    lineHeight: sizes.h4LineHeight,
  },
}

export default {
  buttons,
  gridUnit: '8px',
  lightenPercent: 20,
  ...colors,
  ...sizes,
}
