const ReviewerSuggestion = require('./src/reviewerSuggestion')

module.exports = {
  model: ReviewerSuggestion,
  modelName: 'ReviewerSuggestion',
}
