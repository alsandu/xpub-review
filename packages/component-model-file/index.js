const File = require('./src/file')

module.exports = {
  model: File,
  modelName: 'File',
}
