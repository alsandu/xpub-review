import * as queries from './queries'
import * as fragments from './fragments'
import * as mutations from './mutations'

export { queries, fragments, mutations }

export {
  default as withGQL,
  withAuthorGQL,
  withReviewerGQL,
  withEditorInChiefGQL,
  withHandlingEditorGQL,
} from './withGQL'
