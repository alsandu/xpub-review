import React, { Fragment } from 'react'
import { withModal } from 'component-modal'
import { compose, withHandlers } from 'recompose'

import {
  MultiAction,
  ReviewersTable,
  InviteReviewersForm,
} from 'component-hindawi-ui'

const InviteReviewers = ({
  onInvite,
  reviewers,
  canInviteReviewers,
  cancelReviewerInvitation,
  resendReviewerInvitation,
  canCancelReviewerInvitation,
}) => (
  <Fragment>
    {canInviteReviewers && <InviteReviewersForm onInvite={onInvite} />}
    <ReviewersTable
      canCancelReviewerInvitation={canCancelReviewerInvitation}
      onCancelReviewerInvitation={cancelReviewerInvitation}
      onResendReviewerInvitation={resendReviewerInvitation}
      reviewers={reviewers}
    />
  </Fragment>
)

export default compose(
  withModal({
    modalKey: 'invite-reviewer',
    component: MultiAction,
  }),
  withHandlers({
    onInvite: ({ showModal, onInvite }) => (values, formProps) => {
      showModal({
        title: 'Send Invitation to Review?',
        confirmText: 'SEND',
        cancelText: 'BACK',
        onConfirm: modalProps => onInvite(values, { ...modalProps, formProps }),
      })
    },
  }),
)(InviteReviewers)
