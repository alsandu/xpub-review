import React from 'react'
import { compose, withProps, withHandlers } from 'recompose'
import { get } from 'lodash'
import styled from 'styled-components'
import { Formik } from 'formik'
import { th } from '@pubsweet/ui-toolkit'
import { Button } from '@pubsweet/ui'
import { withJournal } from 'xpub-journal'
import { withModal } from 'component-modal'
import { withGQL as withFilesGQL } from 'component-files/client'

import {
  Row,
  MultiAction,
  marginHelper,
  paddingHelper,
  ContextualBox,
} from 'component-hindawi-ui'
import { ManuscriptFiles, RespondToReviewer, DetailsAndAuthors } from './'
import { autosaveForm, setInitialValues, validateRevision } from '../utils'

const SubmitRevision = ({
  mt,
  match,
  journal,
  validate,
  onSubmit,
  highlight,
  isVisible,
  editAuthor,
  removeAuthor,
  autosaveForm,
  handleUpload,
  handleDelete,
  initialValues,
  revisionDraft,
  updateManuscriptFile,
  revisionManuscriptId,
  addAuthorToManuscript,
}) =>
  isVisible ? (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validate={validateRevision}
    >
      {({ values, errors, clearError, handleSubmit, setFieldValue }) => (
        <ContextualBox highlight={highlight} label="Submit Revision" mt={mt}>
          <Root pr={2} pt={1}>
            {autosaveForm(values)}
            <DetailsAndAuthors
              addAuthorToManuscript={addAuthorToManuscript}
              editAuthor={editAuthor}
              formErrors={errors}
              formValues={values}
              journal={journal}
              removeAuthor={removeAuthor}
              revisionManuscriptId={revisionManuscriptId}
              setFieldValue={setFieldValue}
              startExpanded
            />
            <ManuscriptFiles
              formErrors={errors}
              formValues={values}
              handleDelete={handleDelete}
              handleUpload={handleUpload}
              revisionManuscriptId={revisionManuscriptId}
              startExpanded
              updateManuscriptFile={updateManuscriptFile}
            />
            <RespondToReviewer
              clearError={clearError}
              formValues={values}
              handleDelete={handleDelete}
              handleUpload={handleUpload}
              revisionDraft={revisionDraft}
              setFieldValue={setFieldValue}
              startExpanded
            />
            <Row justify="flex-end" mb={2}>
              <Button
                data-test-id="submit-eic-decision"
                onClick={handleSubmit}
                primary
                size="xLarge"
              >
                Submit revision
              </Button>
            </Row>
          </Root>
        </ContextualBox>
      )}
    </Formik>
  ) : null

export default compose(
  withJournal,
  withFilesGQL({ refetchQueries: ['getDraftRevision'] }),
  withModal({
    component: MultiAction,
    modalKey: 'submitRevision',
  }),
  withProps(({ revisionDraft, journal }) => ({
    initialValues: setInitialValues(revisionDraft),
    revisionManuscriptId: get(revisionDraft, 'id'),
  })),
  withHandlers({
    autosaveForm: ({
      updateAutosave,
      match,
      updateDraftRevision,
    }) => values => {
      autosaveForm({
        values,
        updateDraftRevision,
        updateAutosave,
      })
    },
    onSubmit: ({ onSubmit, showModal }) => (values, formikBag) =>
      showModal({
        cancelText: 'Not yet',
        confirmText: 'Submit revision',
        onConfirm: modalProps => onSubmit(values, modalProps, formikBag),
        subtitle: 'Once submitted, no further changes can be made.',
        title: 'Ready to Submit your Revision?',
      }),
    handleUpload: ({ uploadFile }) => (
      file,
      { type, push, entityId, setFetching, clearError, setFileField },
    ) => {
      const fileInput = {
        type,
        size: file.size,
      }

      clearError && clearError()
      setFetching(true)

      uploadFile({ entityId, fileInput, file }).then(uploadedFile => {
        setFetching(false)
        push && push(uploadedFile)
        setFileField && setFileField(uploadedFile)
      })
    },
    handleDelete: ({ deleteFile }) => (
      file,
      { index, remove, setError, setFetching, setFileField },
    ) => {
      setFetching(true)
      deleteFile(file.id)
        .then(() => {
          setFetching(false)
          remove && remove(index)
          setFileField && setFileField()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(SubmitRevision)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};

  ${marginHelper}
  ${paddingHelper}
`
