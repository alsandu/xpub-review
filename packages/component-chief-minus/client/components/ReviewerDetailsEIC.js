import React from 'react'
import {
  ReviewerDetails,
  ReviewerReports,
  ReviewersTable,
} from 'component-hindawi-ui'

const ReviewerDetailsEIC = ({
  mt,
  options,
  isVisible,
  reviewers,
  manuscript,
  startExpanded,
  reviewerReports,
  canInviteReviewers,
  cancelReviewerInvitation,
  resendReviewerInvitation,
}) => (
  <ReviewerDetails
    isVisible={isVisible}
    mt={mt}
    reviewers={reviewers}
    startExpanded={startExpanded}
    tabButtons={['Reviewer Details', 'Reviewer Reports']}
  >
    <ReviewersTable
      canCancelReviewerInvitation={false}
      onCancelReviewerInvitation={cancelReviewerInvitation}
      onResendReviewerInvitation={resendReviewerInvitation}
      reviewers={reviewers}
    />

    <ReviewerReports options={options} reviewerReports={reviewerReports} />
  </ReviewerDetails>
)

export default ReviewerDetailsEIC
