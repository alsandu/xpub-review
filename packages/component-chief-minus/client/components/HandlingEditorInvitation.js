import React from 'react'
import { Button } from '@pubsweet/ui'
import { Row, PersonInvitation } from 'component-hindawi-ui'

const HandlingEditorInvitation = ({
  toggle,
  handlingEditor,
  cancelInvitation,
  resendInvitation,
}) => (
  <Row justify="flex-start">
    <PersonInvitation
      invitation={handlingEditor}
      label="Handling Editor"
      onResend={resendInvitation}
      onRevoke={cancelInvitation}
    />
    {!handlingEditor.id && (
      <Button ml={1} onClick={toggle} primary size="small">
        INVITE
      </Button>
    )}
  </Row>
)

export default HandlingEditorInvitation
