import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { EditorialReportCard, ContextualBox } from 'component-hindawi-ui'

const EditorialComments = ({
  reviews,
  decisions,
  isVisible,
  editorialReviews,
  ...rest
}) =>
  isVisible ? (
    <ContextualBox label="Editorial Comments" {...rest}>
      <Root>
        {editorialReviews.map(r => (
          <EditorialReportCard
            decisions={decisions}
            key={r.id}
            mb={1}
            privateLabel="Message For Editorial Team"
            publicLabel="Message For Author"
            report={r}
          />
        ))}
      </Root>
    </ContextualBox>
  ) : null

export default EditorialComments

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};

  padding: calc(${th('gridUnit')});
  padding-bottom: 0;
`
