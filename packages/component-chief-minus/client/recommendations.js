export default [
  {
    label: 'Publish',
    value: 'publish',
    message: 'Recommend to Publish',
    button: 'Submit Recommendation',
  },
  {
    label: 'Minor Revision',
    value: 'minor',
    message: 'Minor Revision Requested',
    button: 'Request Revision',
  },
  {
    label: 'Major Revision',
    value: 'major',
    message: 'Major Revision Requested',
    button: 'Request Revision',
  },
  {
    label: 'Reject',
    value: 'reject',
    message: 'Recommend to Reject',
    button: 'Submit Recommendation',
  },
]
