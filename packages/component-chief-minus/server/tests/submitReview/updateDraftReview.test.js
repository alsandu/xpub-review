process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const Chance = require('chance')

const { updateDraftReviewUseCase } = require('../../src/use-cases/submitReview')

const chance = new Chance()

describe('Update Draft Review', () => {
  it('should add comments and recommendation', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {},
      role: 'reviewer',
    })

    const review = fixtures.generateReview({ teamMemberId: teamMember.id })
    const comment = fixtures.generateComment({
      reviewId: review.id,
      type: 'public',
      content: chance.sentence(),
    })

    review.member = teamMember
    review.comments = [comment]

    const mockedModels = models.build(fixtures)

    const input = {
      recommendation: chance.pickone(['minor', 'major', 'reject', 'publish']),
      open: true,
      submitted: false,
      comments: [
        {
          id: comment.id,
          type: 'public',
          content: chance.sentence(),
        },
      ],
    }
    const result = await updateDraftReviewUseCase
      .initialize({
        models: mockedModels,
      })
      .execute({
        reviewId: review.id,
        input,
        userId: teamMember.user.id,
      })
    expect(result.comments[0].type).toContain('public')
    expect(result.comments[0].content).not.toEqual(null)
    expect(['minor', 'major', 'reject', 'publish']).toContain(
      result.recommendation,
    )
  })

  it('should throw an error if user is not allowed to', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {},
      role: 'reviewer',
    })

    const review = fixtures.generateReview({ teamMemberId: teamMember.id })
    const comment = fixtures.generateComment({
      reviewId: review.id,
      type: 'public',
      content: chance.sentence(),
    })

    review.member = teamMember
    review.comments = [comment]

    const mockedModels = models.build(fixtures)

    const input = {
      recommendation: chance.pickone(['minor', 'major', 'reject', 'publish']),
      open: true,
      submitted: false,
      comments: [
        {
          id: comment.id,
          type: 'public',
          content: chance.sentence(),
        },
      ],
    }
    try {
      const result = await updateDraftReviewUseCase
        .initialize({
          models: mockedModels,
        })
        .execute({
          reviewId: review.id,
          input,
          userId: teamMember.user.id,
        })
      expect(result).toBeDefined()
    } catch (e) {
      expect(e.message).toThrow('User can not update selected review')
    }
  })
})
