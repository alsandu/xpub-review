process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { resendHandlingEditorInvitationUseCase } = require('../../src/use-cases')

const notification = {
  sendInvitedHEEmail: jest.fn(),
}
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_resent: 'invitation_resent',
}
logEvent.objectType = { user: 'user' }

describe('Resend handlign editor invitation', () => {
  it('resends an invitation to a handling editor', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const heTeamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'handlingEditor',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    await resendHandlingEditorInvitationUseCase
      .initialize({ notification, models: mockedModels, logEvent })
      .execute({ teamMemberId: heTeamMember.id })

    expect(notification.sendInvitedHEEmail).toHaveBeenCalledTimes(1)
  })
})
