process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const { models, fixtures } = require('fixture-service')

const { getHandlingEditorsUseCase } = require('../../src/use-cases')

describe('Get handling editors', () => {
  it('returns an empty array if there are no global HE teams', async () => {
    const mockedModels = models.build(fixtures)
    const manuscript = fixtures.generateManuscript({})
    const handlingEditorMembers = await getHandlingEditorsUseCase
      .initialize(mockedModels)
      .execute(manuscript.id)

    expect(handlingEditorMembers).toHaveLength(0)
  })
  it('returns all handling editors except those that declined', async () => {
    let globalHeTeam
    globalHeTeam = fixtures.getTeamByRole('handlingEditor')
    if (!globalHeTeam)
      globalHeTeam = fixtures.generateTeam({ role: 'handlingEditor' })

    fixtures.generateUser({})
    fixtures.generateUser({})

    const handlingEditor = fixtures.generateUser({})
    const teamMember = fixtures.generateTeamMember({
      userId: handlingEditor.id,
      teamId: globalHeTeam.id,
    })
    teamMember.user = handlingEditor
    teamMember.linkUser(handlingEditor)
    globalHeTeam.members.push(teamMember)

    // adding a declined HE to a manuscript
    const declinedHe = fixtures.generateUser({})
    const manuscript = fixtures.generateManuscript({})
    const manuscriptTeam = fixtures.generateTeam({
      role: 'handlingEditor',
      manuscriptId: manuscript.id,
    })

    const declinedTeamMember = fixtures.generateTeamMember({
      userId: declinedHe.id,
      teamId: manuscriptTeam.id,
      status: 'declined',
    })

    declinedTeamMember.user = declinedHe
    declinedTeamMember.linkUser(declinedHe)
    manuscriptTeam.members.push(declinedTeamMember)
    manuscript.teams.push(manuscriptTeam)

    // adding the declined HE to the global HE team
    const globalDeclinedTeamMember = fixtures.generateTeamMember({
      userId: declinedHe.id,
      teamId: globalHeTeam.id,
    })

    globalDeclinedTeamMember.user = declinedHe
    globalDeclinedTeamMember.linkUser(declinedHe)
    globalHeTeam.members.push(globalDeclinedTeamMember)

    const mockedModels = models.build(fixtures)

    const handlingEditorMembers = await getHandlingEditorsUseCase
      .initialize(mockedModels)
      .execute(manuscript.id)

    expect(handlingEditorMembers).toHaveLength(1)
    expect(handlingEditorMembers[0].userId).toEqual(handlingEditor.id)
  })
})
