process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { acceptHandlingEditorInvitationUseCase } = require('../../src/use-cases')

const notification = {
  notifyEiCAboutHEDecisionEmail: jest.fn(),
}
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_agreed: 'invitation_agreed',
}
logEvent.objectType = { manuscript: 'manuscript' }

describe('Accept invitation', () => {
  it('accepts an invitation when the input is correct', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending' },
      role: 'handlingEditor',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    await acceptHandlingEditorInvitationUseCase
      .initialize({ notification, models: mockedModels, logEvent })
      .execute({ teamMemberId: teamMember.id })

    expect(teamMember.status).toEqual('accepted')
    expect(notification.notifyEiCAboutHEDecisionEmail).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(
      mockedModels.Manuscript.Statuses.heAssigned,
    )
  })
})
