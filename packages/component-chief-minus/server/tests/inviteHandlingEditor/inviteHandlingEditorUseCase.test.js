process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')

const chance = new Chance()
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { inviteHandlingEditorUseCase } = require('../../src/use-cases')

const notification = {
  sendInvitedHEEmail: jest.fn(),
}
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_sent: 'invitation_sent',
}
logEvent.objectType = { user: 'user' }

describe('Invite handling editor', () => {
  it('adds a user in the handling editor team', async () => {
    const journal = fixtures.journals[0]
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const handlingEditor = fixtures.generateUser({})
    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const manuscriptTeam = fixtures.generateTeam({
      role: 'handlingEditor',
      manuscriptId: manuscript.id,
    })
    await manuscript.assignTeam(manuscriptTeam)

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    await inviteHandlingEditorUseCase
      .initialize({ notification, models: mockedModels, logEvent })
      .execute({
        submissionId: manuscript.submissionId,
        userId: handlingEditor.id,
        reqUserId: editorInChief.id,
      })
    expect(manuscriptTeam.members).toHaveLength(1)
    expect(manuscriptTeam.members[0].userId).toEqual(handlingEditor.id)
    expect(notification.sendInvitedHEEmail).toHaveBeenCalledTimes(1)
  })

  it('creates a team if one does not already exist', async () => {
    const journal = fixtures.journals[0]
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    const handlingEditor = fixtures.generateUser({})
    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    const result = inviteHandlingEditorUseCase
      .initialize({ notification, models: mockedModels, logEvent })
      .execute({
        submissionId: manuscript.submissionId,
        userId: handlingEditor.id,
        reqUserId: editorInChief.id,
      })

    expect(result).resolves.not.toThrow()
    expect(notification.sendInvitedHEEmail).toHaveBeenCalledTimes(1)
  })

  it('return an error if the user is already invited', async () => {
    const journal = fixtures.journals[0]
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    const handlingEditor = fixtures.generateUser({})
    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const manuscriptTeam = fixtures.generateTeam({
      role: 'handlingEditor',
      manuscriptId: manuscript.id,
    })

    const teamMember = fixtures.generateTeamMember({
      userId: handlingEditor.id,
      teamId: manuscriptTeam.id,
    })
    teamMember.user = handlingEditor
    manuscriptTeam.members.push(teamMember)
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    const result = inviteHandlingEditorUseCase
      .initialize({ notification, models: mockedModels, logEvent })
      .execute({
        submissionId: manuscript.submissionId,
        userId: handlingEditor.id,
        reqUserId: editorInChief.id,
      })

    expect(result).rejects.toThrow(
      `User ${
        handlingEditor.identities[0].email
      } is already invited in the handlingEditor team`,
    )
  })
  it('should return an error if there are no manuscripts found', async () => {
    const mockedModels = models.build(fixtures)
    const journal = fixtures.journals[0]
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const result = inviteHandlingEditorUseCase
      .initialize({ notification, models: mockedModels, logEvent })
      .execute({
        submissionId: chance.guid(),
        userId: chance.guid(),
        reqUserId: editorInChief.id,
      })

    expect(result).rejects.toThrow('No manuscript has been found.')
  })
})
