process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  recommendation_accept: 'recommended to Publish manuscript',
}
logEvent.objectType = { manuscript: 'manuscript' }

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  requestRevisionAsHEUseCase,
} = require('../../src/use-cases/recommendAsHE')

const notification = {
  notifySAWhenHERequestsRevision: jest.fn(),
  notifyEiCWhenHEMakesRecommendation: jest.fn(),
  notifyReviewersWhenHEMakesRecommendation: jest.fn(),
}

const jobsService = {
  deletePendingReviewersFromQueue: jest.fn(),
}

const chance = new Chance()
describe('Request revision as HE', () => {
  it('changes the status of a manuscript', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const submissionId = chance.guid()
    const manuscript = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 1,
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending' },
      role: 'reviewer',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'reviewer',
    })

    const heMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'handlingEditor',
    })

    const mockedModels = models.build(fixtures)

    await requestRevisionAsHEUseCase
      .initialize({ notification, models: mockedModels, jobsService, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: heMember.userId,
        comment: chance.sentence(),
        type: chance.pickone(['minor', 'major']),
      })

    expect(manuscript.status).toEqual('revisionRequested')
    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === submissionId,
    )
    expect(manuscripts).toHaveLength(2)
    expect(
      manuscripts.find(m => m.version === 2 && m.status === 'draft'),
    ).toBeTruthy()
  })

  it('should send notifications to EiC, Author and reviewers that are pending or accepted', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending' },
      role: 'reviewer',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'reviewer',
    })

    const heMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'handlingEditor',
    })

    const mockedModels = models.build(fixtures)

    await requestRevisionAsHEUseCase
      .initialize({ notification, models: mockedModels, jobsService, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: heMember.userId,
        comment: chance.sentence(),
        type: chance.pickone(['minor', 'major']),
      })

    expect(notification.notifyEiCWhenHEMakesRecommendation).toHaveBeenCalled()
    expect(notification.notifySAWhenHERequestsRevision).toHaveBeenCalled()
    expect(
      notification.notifyReviewersWhenHEMakesRecommendation,
    ).toHaveBeenCalled()
  })
})
