process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { resendReviewerInvitationUseCase } = require('../../src/use-cases')

const invitation = {
  sendReviewInvitations: jest.fn(),
}

describe('Resend reviewer invitation', () => {
  it('resend the invitation to user', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    const reviewer = fixtures.generateUser({})
    const identity = fixtures.generateIdentity({ userId: reviewer.id })
    reviewer.assignIdentity(identity)

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const manuscriptTeam = fixtures.generateTeam({
      role: 'reviewer',
      manuscriptId: manuscript.id,
    })
    const teamMember = fixtures.generateTeamMember({
      userId: reviewer.id,
      teamId: manuscriptTeam.id,
    })

    manuscriptTeam.manuscript = manuscript
    teamMember.team = manuscriptTeam

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    const mockedModels = models.build(fixtures)

    const input = {
      teamMemberId: teamMember.id,
      manuscriptId: manuscript.id,
    }

    await resendReviewerInvitationUseCase
      .initialize({ invitation, models: mockedModels })
      .execute(input)

    expect(invitation.sendReviewInvitations).toHaveBeenCalledTimes(1)
  })
})
