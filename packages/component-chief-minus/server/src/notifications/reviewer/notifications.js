const config = require('config')
const { get } = require('lodash')
const Email = require('@pubsweet/component-email-templating')

const unsubscribeSlug = config.get('unsubscribe.url')
const staffEmail = config.get('journal.staffEmail')
const journalName = config.get('journal.name')

const { services } = require('helper-service')

const baseUrl = config.get('pubsweet-client.baseUrl')

const { getEmailCopy } = require('./emailCopy')

module.exports = {
  sendHandlingEditorEmail: async ({
    manuscript,
    user,
    isAccepted = true,
    editorInChief,
    submittingAuthor,
  }) => {
    const { title } = manuscript

    const titleText = `the manuscript titled "${title}" by ${get(
      submittingAuthor,
      'alias.givenNames',
      '',
    )} ${get(submittingAuthor, 'alias.surname', '')}`

    const handlingEditorTeam = get(manuscript, 'teams').find(
      t => t.role === 'handlingEditor',
    )
    const handlingEditor = get(handlingEditorTeam, 'members[0]')
    const handlingEditorName = `${get(handlingEditor, 'alias.surname')} ${get(
      handlingEditor,
      'alias.givenNames',
    )}`

    const editorInChiefName = `${get(
      editorInChief,
      'alias.givenNames',
      '',
    )} ${get(editorInChief, 'alias.surname', '')}`

    const emailType = isAccepted ? 'reviewer-accepted' : 'reviewer-declined'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      expectedDate: services.getExpectedDate({
        timestamp: Date.now(),
        daysExpected: 14,
      }),
      targetUserName: user.alias.surname,
    })

    const email = new Email({
      fromEmail: `${editorInChiefName} <${staffEmail}>`,
      type: 'user',
      toUser: {
        email: handlingEditor.alias.email,
        name: handlingEditorName,
      },
      content: {
        subject: isAccepted
          ? `${manuscript.customId}: A reviewer has agreed`
          : `${manuscript.customId}: A reviewer has declined`,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        signatureJournal: '',
        signatureName: editorInChiefName,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: handlingEditor.user.id,
          token: handlingEditor.user.unsubscribeToken,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  sendReviewerEmail: async ({
    user,
    manuscript,
    submittingAuthor,
    isCanceled = false,
  }) => {
    const { title } = manuscript

    const titleText = `the manuscript titled "${title}" by ${get(
      submittingAuthor,
      'alias.givenNames',
      '',
    )} ${get(submittingAuthor, 'alias.surname')}`

    const handlingEditorTeam = get(manuscript, 'teams').find(
      t => t.role === 'handlingEditor',
    )
    const handlingEditor = get(handlingEditorTeam, 'members[0]')
    const handlingEditorName = `${get(handlingEditor, 'alias.surname')} ${get(
      handlingEditor,
      'alias.givenNames',
    )}`

    const emailType = isCanceled
      ? 'reviewer-cancel-invitation'
      : 'reviewer-thank-you'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
    })

    const email = new Email({
      fromEmail: `${handlingEditorName} <${staffEmail}>`,
      type: 'user',
      toUser: {
        email: user.alias.email,
        name: `${user.alias.surname}`,
      },
      content: {
        subject: isCanceled
          ? `${manuscript.customId}: Review no longer required`
          : `${manuscript.customId}: Thank you for agreeing to review`,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        signatureJournal: journalName,
        signatureName: handlingEditorName,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
