const config = require('config')
const { get } = require('lodash')
const { services } = require('helper-service')

const Email = require('@pubsweet/component-email-templating')

const baseUrl = config.get('pubsweet-client.baseUrl')
const { name: journalName, staffEmail } = config.get('journal')
const { getEmailCopy } = require('./emailCopy')

const unsubscribeSlug = config.get('unsubscribe.url')

module.exports = {
  async sendInvitedHEEmail({
    user,
    editorInChief,
    manuscript,
    submittingAuthor,
    isCanceled = false,
  }) {
    const submittingAuthorName = `${get(
      submittingAuthor,
      'alias.givenNames',
      '',
    )} ${get(submittingAuthor, 'alias.surname', '')}`

    const titleText = `the manuscript titled "${get(
      manuscript,
      'title',
      '',
    )}" by ${submittingAuthorName}`

    const editorInChiefName = `${get(
      editorInChief,
      'alias.givenNames',
      '',
    )} ${get(editorInChief, 'alias.surname', '')}`

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      targetUserName: editorInChiefName,
      emailType: isCanceled ? 'he-revoked' : 'he-assigned',
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: get(user, 'alias.email', ''),
        name: get(user, 'alias.surname', ''),
      },
      content: {
        subject: isCanceled
          ? `${manuscript.customId}: Editor invitation cancelled`
          : `${manuscript.customId}: Invitation to edit a manuscript`,
        paragraph,
        signatureName: editorInChiefName,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(user, 'user.id', ''),
          token: get(user, 'user.unsubscribeToken', ''),
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifyAuthorWhenHERemoved({
    manuscript,
    submittingAuthor,
    editorInChief,
  }) {
    const titleText = get(manuscript, 'title', '')
    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      emailType: 'author-he-removed',
    })

    const editorInChiefName = `${get(
      editorInChief,
      'alias.givenNames',
      '',
    )} ${get(editorInChief, 'alias.surname', '')}`

    const email = new Email({
      type: 'user',
      fromEmail: `${editorInChiefName} <${staffEmail}>`,
      toUser: {
        email: get(submittingAuthor, 'alias.email', ''),
        name: `${get(submittingAuthor, 'alias.surname', '')}`,
      },
      content: {
        subject: `${get(
          manuscript,
          'customId',
          '',
        )}:  Your manuscript's editor was changed`,
        paragraph,
        signatureName: editorInChiefName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(submittingAuthor, 'user.id', ''),
          token: get(submittingAuthor, 'user.unsubscribeToken', ''),
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifyInvitedHEWhenRemoved({ user, manuscript, editorInChief }) {
    const titleText = manuscript.title

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      emailType: 'he-he-removed',
    })
    const editorInChiefName = `${get(
      editorInChief,
      'alias.givenNames',
      '',
    )} ${get(editorInChief, 'alias.surname', '')}`

    const email = new Email({
      type: 'user',
      fromEmail: `${editorInChiefName} <${staffEmail}>`,
      toUser: {
        email: get(user, 'alias.email'),
        name: `${get(user, 'alias.surname')}`,
      },
      content: {
        subject: `${get(
          manuscript,
          'customId',
          '',
        )}: The editor in chief removed you from ${titleText}`,
        paragraph,
        signatureName: editorInChiefName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(user, 'user.id', ''),
          token: get(user, 'user.unsubscribeToken'),
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifyEiCAboutHEDecisionEmail({
    user,
    isAccepted = true,
    manuscript,
    submittingAuthor,
    reason,
    editorInChief,
  }) {
    const { title, customId } = manuscript

    const submittingAuthorName = `${get(
      submittingAuthor,
      'alias.givenNames',
      '',
    )} ${get(submittingAuthor, 'alias.surname', '')}`

    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const emailType = isAccepted ? 'he-accepted' : 'he-declined'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      comments: reason ? `Reason: "${reason}"` : '',
      targetUserName: `${get(user, 'alias.surname', '')}`,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: get(editorInChief, 'alias.email', ''),
      },
      content: {
        subject: isAccepted
          ? `${customId}: Editor invitation accepted`
          : `${customId}: Editor invitation declined`,
        paragraph,
        signatureName: editorInChief,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: services.createUrl(baseUrl),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifyAuthorsWhenEiCMakesDecisionToReject({
    authors,
    comment: { content },
    editorInChief,
    manuscript: { id, customId, title, submissionId },
    submittingAuthor,
  }) {
    const submittingAuthorName = submittingAuthor.getName()
    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'authors-manuscript-rejected-before-review',
      comments: content,
    })
    const eicName = editorInChief.getName()

    authors.forEach(async author => {
      const email = new Email({
        type: 'user',
        toUser: {
          email: get(author, 'alias.email', ''),
          name: get(author, 'alias.surname', ''),
        },
        fromEmail: `${eicName} <${staffEmail}>`,
        content: {
          subject: `${customId}: Manuscript rejected`,
          paragraph,
          signatureName: eicName,
          ctaText: 'MANUSCRIPT DETAILS',
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: get(author, 'user.id', ''),
            token: get(author, 'user.unsubscribeToken', ''),
          }),
          ctaLink: services.createUrl(
            baseUrl,
            `/details/${submissionId}/${id}`,
          ),
          signatureJournal: journalName,
        },
        bodyProps,
      })

      return email.sendEmail()
    })
  },
  async notifySAWhenEiCRequestsRevision({
    manuscript,
    submittingAuthor,
    review,
    editorInChief,
  }) {
    const eicName = editorInChief.getName()

    const authorNote = review.comments.find(comm => comm.type === 'public')
    const content = get(authorNote, 'content')

    const authorNoteText = content ? `Reason & Details: "${content}"` : ''

    const titleText = `the manuscript titled "${manuscript.title}" by ${
      submittingAuthor.alias.givenNames
    } ${submittingAuthor.alias.surname}`

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'author-request-to-revision-from-eic',
      titleText,
      comments: authorNoteText,
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: submittingAuthor.alias.email,
        name: submittingAuthor.alias.surname,
      },
      fromEmail: `${eicName} <${staffEmail}>`,
      content: {
        subject: `${manuscript.customId}: Revision requested`,
        paragraph,
        signatureName: eicName,
        ctaText: 'MANUSCRIPT DETAILS',
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifyEiCWhenHEMakesRecommendation({
    review,
    editorInChief,
    manuscript,
    commentForEiC,
    handlingEditor,
    submittingAuthor,
  }) {
    let emailType, subject
    switch (review.recommendation) {
      case 'minor':
      case 'major':
        emailType = 'eic-request-revision-from-he'
        subject = `${manuscript.customId}: Revision requested`
        break
      case 'reject':
        emailType = 'eic-recommend-to-reject-from-he'
        subject = `${manuscript.customId}: Recommendation to reject`
        break
      case 'publish':
        emailType = 'eic-recommend-to-publish-from-he'
        subject = `${manuscript.customId}: Recommendation to publish`
        break
      default:
        throw new Error(`undefined recommendation: ${review.recommendation} `)
    }

    const comments = commentForEiC
      ? `The editor provided the following comments: "${commentForEiC}"`
      : ''

    const { paragraph, ...bodyProps } = getEmailCopy({
      comments,
      emailType,
      titleText: `the manuscript titled "${manuscript.title}" by ${
        submittingAuthor.alias.givenNames
      } ${submittingAuthor.alias.surname}`,
      targetUserName: handlingEditor.getLastName(),
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: editorInChief.alias.email,
        name: editorInChief.getName(),
      },
      fromEmail: `${journalName} <${staffEmail}>`,
      content: {
        subject,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: baseUrl,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifySAWhenHERequestsRevision({
    manuscript,
    commentForAuthor,
    handlingEditor,
    submittingAuthor,
  }) {
    const signatureName = handlingEditor.getName()
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'author-request-to-revision',
      titleText: `your submission "${manuscript.title}" to ${journalName}`,
      comments: commentForAuthor,
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: submittingAuthor.alias.email,
        name: submittingAuthor.getLastName(),
      },
      fromEmail: `${signatureName} <${staffEmail}>`,
      content: {
        subject: `${manuscript.customId}: Revision requested`,
        paragraph,
        signatureName,
        ctaText: 'MANUSCRIPT DETAILS',
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifyHeWhenRevisionSubmitted({
    draft,
    submittingAuthor,
    handlingEditor,
    submissionId,
  }) {
    const titleText = get(draft, 'title', '')
    const targetUserName = ` ${get(
      submittingAuthor,
      'alias.surname',
      '',
    )} ${get(submittingAuthor, 'alias.givenNames', '')}`
    const { paragraph } = getEmailCopy({
      titleText,
      emailType: 'revision-submitted',
      targetUserName,
    })
    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: get(handlingEditor, 'alias.email', ''),
      },
      content: {
        subject: `${draft.customId}: Revision submitted`,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${submissionId}/${draft.id}`,
        ),
      },
    })
    return email.sendEmail()
  },
  async notifyHEWhenReviewerSubmitsReview({
    reviewer,
    manuscript,
    editorInChief,
    handlingEditor,
    submittingAuthor,
  }) {
    const submittingAuthorName = `${get(
      submittingAuthor,
      'alias.givenNames',
      '',
    )} ${get(submittingAuthor, 'alias.surname', '')}`

    const titleText = `the manuscript titled "${get(
      manuscript,
      'title',
      '',
    )}" by ${submittingAuthorName}`

    const editorInChiefName = `${get(
      editorInChief,
      'alias.givenNames',
      '',
    )} ${get(editorInChief, 'alias.surname', '')}`

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      targetUserName: `${get(reviewer, 'alias.surname', '')}`,
      emailType: 'reviewer-submitted-review',
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: handlingEditor.alias.email,
        name: handlingEditor.getLastName(),
      },
      fromEmail: `${journalName} <${staffEmail}>`,
      content: {
        subject: `${manuscript.customId}: A review has been submitted`,
        paragraph,
        signatureName: editorInChiefName,
        ctaText: 'MANUSCRIPT DETAILS',
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: handlingEditor.user.id,
          token: handlingEditor.user.unsubscribeToken,
        }),
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifyReviewersWhenHERemoved({ reviewers, manuscript, editorInChief }) {
    const { customId, title } = manuscript

    const { paragraph, ...bodyProps } = getEmailCopy({
      title,
      emailType: 'reviewer-he-removed',
    })
    const eicName = editorInChief.getName()
    const filteredReviewers = reviewers.filter(r => r.status !== 'declined')

    filteredReviewers.forEach(reviewer => {
      const email = new Email({
        type: 'user',
        toUser: {
          email: get(reviewer, 'alias.email', ''),
          name: get(reviewer, 'alias.surname', ''),
        },
        fromEmail: `${eicName} <${staffEmail}>`,
        content: {
          subject: `${customId}: The handling editor of a manuscript that you were reviewing was changed`,
          paragraph,
          signatureName: eicName,
          signatureJournal: journalName,
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: reviewer.user.id,
            token: reviewer.user.unsubscribeToken,
          }),
        },
        bodyProps,
      })
      return email.sendEmail()
    })
  },

  async notifyReviewersWhenHEMakesRecommendation({
    submittingAuthor,
    handlingEditor,
    manuscript,
    reviewers,
  }) {
    const titleText = `the manuscript titled "${
      manuscript.title
    }" by ${submittingAuthor.getName()}`

    const signatureName = handlingEditor.getName()

    const acceptedReviewers = reviewers.filter(
      reviewer => reviewer.status === 'accepted',
    )

    const acceptedReviewersEmailBody = getEmailCopy({
      emailType: 'accepted-reviewers-after-recommendation',
      titleText,
    })

    const pendingReviewers = reviewers.filter(
      reviewer => reviewer.status === 'pending',
    )

    const pendingReviewersEmailBody = getEmailCopy({
      emailType: 'pending-reviewers-after-recommendation',
      titleText,
    })

    const buildSendEmailFunction = emailBodyProps => reviewer => {
      const { paragraph, ...bodyProps } = emailBodyProps

      const email = new Email({
        type: 'user',
        toUser: {
          email: reviewer.alias.email,
          name: reviewer.alias.surname,
        },
        fromEmail: `${signatureName} <${staffEmail}>`,
        content: {
          subject: `${manuscript.customId}: Review no longer required`,
          paragraph,
          signatureName,
          ctaText: 'MANUSCRIPT DETAILS',
          unsubscribeLink: services.createUrl(this.baseUrl, unsubscribeSlug, {
            id: reviewer.id,
            token: reviewer.unsubscribeToken,
          }),
          ctaLink: services.createUrl(
            this.baseUrl,
            `/details/${manuscript.id}`,
          ),
          signatureJournal: journalName,
        },
        bodyProps,
      })

      return email.sendEmail()
    }

    return Promise.all([
      ...acceptedReviewers.map(
        buildSendEmailFunction(acceptedReviewersEmailBody),
      ),
      ...pendingReviewers.map(
        buildSendEmailFunction(pendingReviewersEmailBody),
      ),
    ])
  },
}
