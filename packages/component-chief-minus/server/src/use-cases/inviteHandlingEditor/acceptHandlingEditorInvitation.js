const { get, chain } = require('lodash')

const initialize = ({
  notification,
  models: { TeamMember, Journal, Manuscript, Team },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.manuscript.teams.members',
    )
    const manuscript = get(teamMember, 'team.manuscript', {})
    const manuscripts = await Manuscript.findBy(
      {
        submissionId: manuscript.submissionId,
      },
      'teams.[members]',
    )
    const heTeams = manuscripts.map(m =>
      m.teams.find(t => t.role === Team.Role.handlingEditor),
    )

    const teamMembers = chain(heTeams)
      .flatMap(t => t.members)
      .filter(m => m.userId === teamMember.userId)
      .value()
    await Promise.all(
      teamMembers.map(async member => {
        member.updateProperties({ status: TeamMember.Statuses.accepted })
        await member.save()
      }),
    )

    manuscript.updateStatus(Manuscript.Statuses.heAssigned)
    await manuscript.save()

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()

    const submittingAuthor = manuscript.getSubmittingAuthor()

    notification.notifyEiCAboutHEDecisionEmail({
      user: teamMember,
      manuscript,
      submittingAuthor,
      editorInChief,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
