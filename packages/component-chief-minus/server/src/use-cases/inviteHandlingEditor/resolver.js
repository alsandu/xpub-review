const useCases = require('./index')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const notificationsHandlingEditor = require('../../notifications/handlingEditor/notifications')

const resolver = {
  Query: {
    async getHandlingEditors(_, { manuscriptId }, ctx) {
      return useCases.getHandlingEditorsUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
  Mutation: {
    async inviteHandlingEditor(_, { submissionId, userId }, ctx) {
      return useCases.inviteHandlingEditorUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
        })
        .execute({ submissionId, userId, reqUserId: ctx.user })
    },
    async resendHandlingEditorInvitation(_, { teamMemberId }, ctx) {
      return useCases.resendHandlingEditorInvitationUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async cancelHandlingEditorInvitation(_, { teamMemberId }, ctx) {
      return useCases.cancelHandlingEditorInvitationUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async acceptHandlingEditorInvitation(_, { teamMemberId }, ctx) {
      return useCases.acceptHandlingEditorInvitationUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async declineHandlingEditorInvitation(_, input, ctx) {
      return useCases.declineHandlingEditorInvitationUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
        })
        .execute({ input, userId: ctx.user })
    },
    async removeHandlingEditor(_, { teamMemberId }, ctx) {
      return useCases.removeHandlingEditorUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
  },
}

module.exports = resolver
