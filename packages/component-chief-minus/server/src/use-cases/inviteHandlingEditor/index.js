const getHandlingEditorsUseCase = require('./getHandlingEditors')

const inviteHandlingEditorUseCase = require('./inviteHandlingEditor')
const resendHandlingEditorInvitationUseCase = require('./resendHandlingEditorInvitation')
const cancelHandlingEditorInvitationUseCase = require('./cancelHandlingEditorInvitation')
const acceptHandlingEditorInvitationUseCase = require('./acceptHandlingEditorInvitation')
const declineHandlingEditorInvitationUseCase = require('./declineHandlingEditorInvitation')
const removeHandlingEditorUseCase = require('./removeHandlingEditor')

module.exports = {
  getHandlingEditorsUseCase,
  inviteHandlingEditorUseCase,
  resendHandlingEditorInvitationUseCase,
  cancelHandlingEditorInvitationUseCase,
  acceptHandlingEditorInvitationUseCase,
  declineHandlingEditorInvitationUseCase,
  removeHandlingEditorUseCase,
}
