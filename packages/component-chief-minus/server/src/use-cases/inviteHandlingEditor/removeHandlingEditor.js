const { get, flatMap } = require('lodash')
const Promise = require('bluebird')

const initialize = ({
  notification,
  models: { TeamMember, Journal, Manuscript, Team, Review },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.[members, manuscript.teams.members]',
    )

    const manuscript = get(teamMember, 'team.manuscript')
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }

    const reviewersTeam = manuscript.teams.find(
      t => t.role === Team.Role.reviewer,
    )

    let reviewers = []
    if (reviewersTeam) {
      reviewers = reviewersTeam.members
      await reviewersTeam.delete()
    }

    const heRecommendation = await Review.findOneBy({
      queryObject: {
        manuscriptId: manuscript.id,
        teamMemberId,
      },
    })

    if (heRecommendation) {
      await heRecommendation.delete()
    }

    const allVersionsOfManuscript = await Manuscript.findBy(
      { submissionId: manuscript.submissionId },
      'teams.members',
    )

    const allHETeams = allVersionsOfManuscript.map(manuscript =>
      manuscript.teams.find(t => t.role === Team.Role.handlingEditor),
    )

    const allHEMembers = flatMap(allHETeams, t => t.members)

    await Promise.each(allHEMembers, member => {
      member.status = TeamMember.Statuses.removed
      return member.save()
    })

    const draftManuscript = allVersionsOfManuscript.find(
      m => m.status === Manuscript.Statuses.draft,
    )

    if (draftManuscript) {
      await draftManuscript.delete()
    }

    manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    notification.notifyAuthorWhenHERemoved({
      manuscript,
      submittingAuthor,
      editorInChief,
    })

    notification.notifyInvitedHEWhenRemoved({
      user: teamMember,
      manuscript,
      editorInChief,
    })

    notification.notifyReviewersWhenHERemoved({
      reviewers,
      manuscript,
      editorInChief,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.he_removed,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = [
  'authenticatedUser',
  'adminOrEditorInChief',
  'hasAccessToManuscriptVersions',
]

module.exports = {
  initialize,
  authsomePolicies,
}
