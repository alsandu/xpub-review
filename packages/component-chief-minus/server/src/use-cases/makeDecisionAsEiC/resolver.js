const useCases = require('./index')
const { sendPackage } = require('component-mts-package')
const models = require('@pubsweet/models')
const notificationsEditorInChief = require('../../notifications/editorInChief/notifications')
const { logEvent } = require('component-activity-log/server')

const resolver = {
  Query: {},
  Mutation: {
    async makeDecisionToPublishAsEiC(_, { manuscriptId }, ctx) {
      return useCases.makeDecisionToPublishAsEiCUseCase
        .initialize({
          notification: notificationsEditorInChief,
          models,
          sendPackage,
          logEvent,
        })
        .execute({ manuscriptId, userId: ctx.user })
    },
    async makeDecisionToReturnAsEiC(_, { manuscriptId, content }, ctx) {
      return useCases.makeDecisionToReturnAsEiCUseCase
        .initialize({
          notification: notificationsEditorInChief,
          models,
          logEvent,
        })
        .execute({ manuscriptId, content, userId: ctx.user })
    },
    async requestRevisionAsEiC(_, { manuscriptId, content }, ctx) {
      return useCases.requestRevisionAsEiCUseCase
        .initialize({
          notification: notificationsEditorInChief,
          models,
          logEvent,
        })
        .execute({ manuscriptId, content, userId: ctx.user })
    },
    async makeDecisionToRejectAsEiC(_, { manuscriptId, content }, ctx) {
      return useCases.makeDecisionToRejectAsEiCUseCase
        .initialize({
          notification: notificationsEditorInChief,
          models,
          logEvent,
        })
        .execute({ manuscriptId, content, userId: ctx.user })
    },
  },
}

module.exports = resolver
