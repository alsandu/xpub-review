const useCases = require('./index')
const models = require('@pubsweet/models')
const Job = require('component-jobs')
const { logEvent } = require('component-activity-log/server')
const invitationsReviewer = require('../../notifications/reviewer/invitations')
const notificationsReviewer = require('../../notifications/reviewer/notifications')

const resolver = {
  Query: {
    async getReviewersInvited(_, { manuscriptId }, ctx) {
      return useCases.getReviewersInvitedUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
  Mutation: {
    async inviteReviewer(_, { manuscriptId, input }, ctx) {
      return useCases.inviteReviewerUseCase
        .initialize({ invitation: invitationsReviewer, models, logEvent })
        .execute({ manuscriptId, input, reqUserId: ctx.user })
    },
    async resendReviewerInvitation(_, input, ctx) {
      return useCases.resendReviewerInvitationUseCase
        .initialize({ invitation: invitationsReviewer, models })
        .execute(input)
    },
    async cancelReviewerInvitation(_, input, ctx) {
      return useCases.cancelReviewerInvitationUseCase
        .initialize({
          notification: notificationsReviewer,
          models,
          Job,
          logEvent,
        })
        .execute({ input, userId: ctx.user })
    },
    async acceptReviewerInvitation(_, { teamMemberId }, ctx) {
      return useCases.acceptReviewerInvitationUseCase
        .initialize({
          notification: notificationsReviewer,
          models,
          Job,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async declineReviewerInvitation(_, { teamMemberId }, ctx) {
      return useCases.declineReviewerInvitationUseCase
        .initialize({
          notification: notificationsReviewer,
          models,
          Job,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
  },
}

module.exports = resolver
