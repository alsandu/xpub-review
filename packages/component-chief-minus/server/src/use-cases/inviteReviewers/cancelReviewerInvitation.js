const { get } = require('lodash')

const initialize = ({
  Job,
  notification,
  models: { TeamMember, Team, Manuscript, ReviewerSuggestion },
  logEvent,
}) => ({
  async execute({ input: { manuscriptId, teamMemberId }, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.[members,manuscript.teams.members]',
    )

    const { team } = teamMember
    const manuscript = get(team, 'manuscript')

    if (!manuscript) {
      throw new Error('Manuscript does not exist')
    }

    const submittingAuthor = manuscript.getSubmittingAuthor()
    team.removeMember(teamMemberId)
    await team.saveRecursively()

    const manuscriptReviewersTeam = await Team.findOneBy({
      queryObject: { manuscriptId, role: Team.Role.reviewer },
      eagerLoadRelations: 'members',
    })

    const pendingOrAcceptedReviewersTeam = manuscriptReviewersTeam.members.find(
      member =>
        ![TeamMember.Statuses.declined, TeamMember.Statuses.expired].includes(
          member.status,
        ),
    )

    if (!pendingOrAcceptedReviewersTeam) {
      await manuscript.updateStatus(Manuscript.Statuses.heAssigned)
      await manuscript.save()
    }
    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: teamMember.alias.email,
        manuscriptId: manuscript.id,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = false
      await reviewerSuggestion.save()
    }

    if (team.members.length === 0) {
      await team.delete()
    }
    await Job.cancelQueue(`reminders-${teamMemberId}`)
    await Job.cancelQueue(`removal-${teamMemberId}`)

    notification.sendReviewerEmail({
      user: teamMember,
      manuscript,
      submittingAuthor,
      isCanceled: true,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.invitation_revoked,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = [
  'authenticatedUser',
  'handlingEditorOnManuscript',
  'hasAccessToManuscriptVersions',
]

module.exports = {
  initialize,
  authsomePolicies,
}
