const { get } = require('lodash')

const initialize = ({ invitation, models: { TeamMember } }) => ({
  async execute({ teamMemberId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.[manuscript.[teams.[members.[user.[identities]]]]]',
    )
    const manuscript = get(teamMember, 'team.manuscript')
    const submittingAuthor = manuscript.getSubmittingAuthor()

    invitation.sendReviewInvitations({
      manuscript,
      invitation: teamMember,
      submittingAuthor,
      user: teamMember,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
