const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const Job = require('component-jobs')

const useCases = require('./index')
const notificationsHandlingEditor = require('../../notifications/handlingEditor/notifications')

const JobsService = require('../../jobs/jobsService.js')

const resolver = {
  Query: {},
  Mutation: {
    async recommendToRejectAsHE(_, { manuscriptId, input }, ctx) {
      const jobsService = JobsService.initialize({ Job, models })

      return useCases.recommendToRejectAsHEUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
          jobsService,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async requestRevisionAsHE(_, { manuscriptId, content, type }, ctx) {
      const jobsService = JobsService.initialize({ Job, models })

      return useCases.requestRevisionAsHEUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
          jobsService,
        })
        .execute({ manuscriptId, content, userId: ctx.user, type })
    },
    async recommendToPublishAsHE(_, { manuscriptId, input }, ctx) {
      const jobsService = JobsService.initialize({ Job, models })

      return useCases.recommendToPublishAsHEUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
          jobsService,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
  },
}

module.exports = resolver
