const recommendToRejectAsHEUseCase = require('./recommendToRejectAsHE')
const requestRevisionAsHEUseCase = require('./requestRevisionAsHE')
const recommendToPublishAsHEUseCase = require('./recommendToPublishAsHE')

module.exports = {
  recommendToRejectAsHEUseCase,
  requestRevisionAsHEUseCase,
  recommendToPublishAsHEUseCase,
}
