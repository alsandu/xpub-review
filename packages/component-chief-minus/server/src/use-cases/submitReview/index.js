const updateDraftReviewUseCase = require('./updateDraftReview')
const submitReviewUseCase = require('./submitReview')

module.exports = { updateDraftReviewUseCase, submitReviewUseCase }
