const submitRevisionUseCase = require('./submitRevision')
const updateDraftRevisionUseCase = require('./updateDraftRevision')

module.exports = {
  submitRevisionUseCase,
  updateDraftRevisionUseCase,
}
