const useCases = require('./index')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const notificationsHandlingEditor = require('../../notifications/handlingEditor/notifications')
const notificationsEditorInChief = require('../../notifications/editorInChief/notifications')
const invitationsReviewer = require('../../notifications/reviewer/invitations')

const resolver = {
  Query: {},
  Mutation: {
    async submitRevision(_, { submissionId }, ctx) {
      return useCases.submitRevisionUseCase
        .initialize({
          notification: {
            notificationsHandlingEditor,
            notificationsEditorInChief,
            invitationsReviewer,
          },
          models,
          logEvent,
        })
        .execute({ submissionId })
    },
    async updateDraftRevision(_, { manuscriptId, autosaveInput }, ctx) {
      return useCases.updateDraftRevisionUseCase
        .initialize(models)
        .execute({ manuscriptId, autosaveInput })
    },
  },
}

module.exports = resolver
