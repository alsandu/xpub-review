const moment = require('moment')
const { cloneDeep } = require('lodash')
const Job = require('component-jobs')
const logger = require('@pubsweet/logger')
const { logEvent } = require('component-activity-log/server')

const { getEmailCopy } = require('../notifications/reviewer/emailCopy')
const Email = require('@pubsweet/component-email-templating')

const scheduleReminderJob = async ({
  days,
  email,
  order,
  userId,
  subject,
  timeUnit,
  titleText,
  invitationId,
  expectedDate,
  manuscriptId,
}) => {
  const executionDate = moment()
    .add(days, timeUnit)
    .toISOString()

  const queue = `reminders-${invitationId}`

  const { paragraph, ...bodyProps } = getEmailCopy({
    emailType: `reviewer-resend-invitation-${order}-reminder`,
    titleText,
    expectedDate,
  })

  email.bodyProps = bodyProps
  email.content.subject = subject

  const params = {
    days,
    order,
    userId,
    timeUnit,
    manuscriptId,
    executionDate,
    emailProps: cloneDeep(email),
  }

  await Job.schedule({ queue, params, executionDate, jobHandler })
}

const jobHandler = async job => {
  const {
    days,
    order,
    userId,
    timeUnit,
    emailProps,
    manuscriptId,
    executionDate,
  } = job.data
  const email = new Email(emailProps)
  await email.sendEmail()

  logEvent({
    userId: null,
    manuscriptId,
    action: logEvent.actions[`reminder_invitation_${order}`],
    objectType: logEvent.objectType.user,
    objectId: userId,
  })

  const message = `Job ${
    job.id
  }: the ${days} ${timeUnit} reminder has been sent to ${
    email.toUser.email
  } at ${executionDate}`
  logger.info(message)

  return message
}
module.exports = { scheduleReminderJob }
