module.exports.initialize = ({ Job, models: { TeamMember } }) => ({
  async deletePendingReviewersFromQueue({ reviewerTeam }) {
    const pendingReviewers = reviewerTeam.members.filter(
      m => m.status === TeamMember.Statuses.pending,
    )
    return Promise.all(
      pendingReviewers.map(async pendingReviewer => {
        await Job.cancelQueue(`reminders-${pendingReviewer.id}`)
        await Job.cancelQueue(`removal-${pendingReviewer.id}`)
      }),
    )
  },
})
