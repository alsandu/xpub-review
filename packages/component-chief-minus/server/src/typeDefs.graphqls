input ReviewerInput {
  email: String!
  givenNames: String!
  surname: String!
  aff: String
  country: String
}

input RecommendationToRejectAsHEInput {
  forAuthor: String!
  forEiC: String
}

input RecommendationToPublishAsHEInput {
  forAuthor: String
  forEiC: String
}
input RevisionAutosaveInput {
  meta: MetadataInput
  authors: [AuthorInput]
  files: [FileInput]
  content: String
}

input UpdateDraftComment {
  id: String
  type: String
  content: String
}

input UpdateDraftReviewInput {
  recommendation: String
  open: String
  submitted: String
  comments: [UpdateDraftComment]
}

extend type Query {
  # Get all the handling editors eligible for this manuscript.
  getHandlingEditors(manuscriptId: String!): [TeamMember]
  # Get all the reviewers that are invited to review the manuscript
  getReviewersInvited(manuscriptId: String!): [TeamMember]
}

enum AllowedRevisionType {
  minor
  major
}

extend type Mutation {
  #Handling editor recommends to reject.
  recommendToRejectAsHE(
    manuscriptId: String!
    input: RecommendationToRejectAsHEInput
  ): Boolean
  # Handling editor recommends to publish after peer review
  recommendToPublishAsHE(
    manuscriptId: String!
    input: RecommendationToPublishAsHEInput
  ): Boolean
  #Invite a handling editor to a manuscript.
  inviteHandlingEditor(submissionId: String!, userId: String!): Boolean
  # Resend the invitation to the handling editor.
  resendHandlingEditorInvitation(teamMemberId: String!): Boolean
  # Cancel a handling editor invitation to a manuscript.
  cancelHandlingEditorInvitation(teamMemberId: String!): Boolean
  # Accept an invitation as a handling editor.
  acceptHandlingEditorInvitation(teamMemberId: String!): Boolean
  # Decline an invitation as a handling editor.
  declineHandlingEditorInvitation(
    teamMemberId: String!
    reason: String
  ): Boolean

  # Remove a handling editor from a manuscript.
  removeHandlingEditor(teamMemberId: String!): Boolean
  # Invite reviewers to a manuscript
  inviteReviewer(manuscriptId: String!, input: ReviewerInput!): Boolean
  # Resend the invitation to the reviewer
  resendReviewerInvitation(
    manuscriptId: String!
    teamMemberId: String!
  ): Boolean
  # Cancel a reviewer invitation to manuscript
  cancelReviewerInvitation(
    manuscriptId: String!
    teamMemberId: String!
  ): Boolean
  # Accept an invitation as a reviewer
  acceptReviewerInvitation(teamMemberId: String!): Boolean
  # Decline an invitation as a reviewer
  declineReviewerInvitation(teamMemberId: String!): Boolean
  makeDecisionToRejectAsEiC(manuscriptId: String!, content: String!): Boolean
  makeDecisionToReturnAsEiC(manuscriptId: String!, content: String!): Boolean
  makeDecisionToPublishAsEiC(manuscriptId: String!): Boolean

  requestRevisionAsEiC(manuscriptId: String!, content: String!): Boolean
  requestRevisionAsHE(
    manuscriptId: String!
    content: String!
    type: AllowedRevisionType!
  ): Boolean
  #Submit revision.
  submitRevision(submissionId: String!): Boolean
  updateDraftRevision(
    manuscriptId: String!
    autosaveInput: RevisionAutosaveInput
  ): Manuscript
  updateDraftReview(reviewId: String!, input: UpdateDraftReviewInput!): Review
  submitReview(reviewId: String!): Boolean
}
