import React from 'react'

import { OpenModal, ActionLink, IconButton } from 'component-hindawi-ui'

import AdminUserForm from './AdminUserForm'

const OpenUserForm = ({ edit, user, modalKey, onClick }) => (
  <OpenModal
    component={AdminUserForm}
    edit={edit}
    modalKey={modalKey}
    user={user}
  >
    {showModal =>
      edit ? (
        <IconButton fontIcon="editIcon" iconSize={2} onClick={onClick} />
      ) : (
        <ActionLink icon="plus" onClick={onClick}>
          ADD USER
        </ActionLink>
      )
    }
  </OpenModal>
)

export default OpenUserForm
