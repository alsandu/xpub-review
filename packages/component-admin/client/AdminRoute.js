import React from 'react'
import { get } from 'lodash'
import { compose } from 'recompose'
import { Query } from 'react-apollo'
import { Spinner } from '@pubsweet/ui'

import { Redirect, withRouter, Route } from 'react-router-dom'
import { queries } from './graphql'

const AdminRoute = ({ redirectPath = '/', component: Component, ...rest }) => (
  <Query query={queries.currentUser}>
    {({ data, loading }) => {
      if (loading) {
        return <Spinner />
      }
      if (get(data, 'currentUser.role') === 'admin') {
        return <Route render={props => <Component {...props} />} {...rest} />
      }
      return <Redirect to="/" />
    }}
  </Query>
)

export default compose(withRouter)(AdminRoute)
