import React, { Fragment } from 'react'
import { H1 } from '@pubsweet/ui'
import { Row, IconCard } from 'component-hindawi-ui'

import withUsersGQL from '../graphql'

const AdminDashboard = ({ history }) => (
  <Fragment>
    <H1 justify="flex-start" mt={2} pl={12}>
      Admin dashboard
    </H1>
    <Row justify="flex-start" mt={2} pl={12}>
      <IconCard
        icon="users"
        iconSize={6}
        label="Users"
        onClick={() => history.push('/admin/users')}
      />
    </Row>
  </Fragment>
)

export default withUsersGQL(AdminDashboard)
