import gql from 'graphql-tag'

export const userFragment = gql`
  fragment userDetails on User {
    id
    isActive
    role
    identities {
      ... on Local {
        name {
          surname
          givenNames
          title
        }
        email
        aff
        isConfirmed
        country
      }
    }
  }
`
