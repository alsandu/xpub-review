const typeDefs = require('./server/src/typeDefs')
const resolvers = require('./server/src/resolvers')

module.exports = {
  resolvers,
  typeDefs,
}
