process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { editUserFromAdminPanelUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('Edit users from admin panel', () => {
  it('should edit an user information', async () => {
    const input = {
      surname: chance.last(),
      givenNames: chance.first(),
      role: chance.pickone(['admin', 'handlingEditor', 'editorInChief']),
    }
    const user = fixtures.generateUser({})
    const mockedModels = models.build(fixtures)

    await editUserFromAdminPanelUseCase
      .initialize(mockedModels)
      .execute({ input, id: user.id })

    expect(user.identities[0].surname).toEqual(input.surname)
  })

  it('should edit the user`s role', async () => {
    const journal = fixtures.journals[0]
    const { user } = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })
    const mockedModels = models.build(fixtures)

    await editUserFromAdminPanelUseCase
      .initialize(mockedModels)
      .execute({ input: { role: 'admin' }, id: user.id })
    const { members } = fixtures.getTeamByRole('admin')

    expect(members.find(m => m.user.id === user.id).user).toEqual(user)
  })

  it('should return an error when the user id is invalid', async () => {
    const input = {
      surname: chance.last(),
      givenNames: chance.first(),
      role: chance.pickone(['admin', 'handlingEditor', 'editorInChief']),
    }
    const mockedModels = models.build(fixtures)
    const result = editUserFromAdminPanelUseCase
      .initialize(mockedModels)
      .execute({ input, id: chance.guid() })

    expect(result).rejects.toThrow()
  })
})
