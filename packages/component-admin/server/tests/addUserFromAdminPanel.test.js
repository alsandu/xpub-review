process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { cloneDeep } = require('lodash')
const { models, fixtures } = require('fixture-service')

const { addUserFromAdminPanelUseCase } = require('../src/use-cases')

const chance = new Chance()

const notificationService = {
  notifyUserAddedByAdmin: jest.fn(),
}

const basicInput = {
  givenNames: chance.first(),
  surname: chance.first(),
  email: chance.email(),
  country: chance.country(),
  affiliation: chance.company(),
  title: 'mrs',
  role: 'user',
}

describe('Add users from admin panel', () => {
  let input

  beforeEach(() => {
    input = cloneDeep(basicInput)
  })
  it('should create a new user', async () => {
    const mockedModels = models.build(fixtures)
    const initialUsersLength = fixtures.users.length
    await addUserFromAdminPanelUseCase
      .initialize({ notification: notificationService, models: mockedModels })
      .execute(input)

    expect(initialUsersLength).toBeLessThan(fixtures.users.length)

    expect(input.givenNames).toEqual(fixtures.users[0].identities[0].givenNames)
  })
  it('should create a new user and add him in a team', async () => {
    input.role = 'editorInChief'
    input.email = chance.email()
    const mockedModels = models.build(fixtures)

    await addUserFromAdminPanelUseCase
      .initialize({ notification: notificationService, models: mockedModels })
      .execute(input)

    expect(fixtures.teams[0].role).toEqual(input.role)
  })
})
