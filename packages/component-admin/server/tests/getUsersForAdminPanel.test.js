process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const { models, fixtures } = require('fixture-service')

const { getUsersForAdminPanelUseCase } = require('../src/use-cases')

describe('get users for admin panel', () => {
  it('should return the existing users', async () => {
    let team
    team = fixtures.getTeamByRole('admin')
    if (!team) team = fixtures.generateTeam({ role: 'admin' })

    fixtures.generateUser({})
    fixtures.generateUser({})
    const user = fixtures.generateUser({})
    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
    })
    team.members.push(teamMember)
    const mockedModels = models.build(fixtures)

    const users = await getUsersForAdminPanelUseCase
      .initialize(mockedModels)
      .execute()

    expect(users.length).toEqual(3)
    expect(users[0].identities.length).toBeGreaterThan(0)
    expect(users[0].role).toEqual('user')

    const admin = users.find(user => user.id === team.members[0].userId)
    expect(admin.role).toEqual(team.role)
  })
})
