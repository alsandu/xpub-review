const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')
const notification = require('./notifications/notification')

const resolvers = {
  Query: {
    async getUsersForAdminPanel(_, { input }, ctx) {
      return useCases.getUsersForAdminPanelUseCase.initialize(models).execute()
    },
  },
  Mutation: {
    async addUserFromAdminPanel(_, { input }, ctx) {
      return useCases.addUserFromAdminPanelUseCase
        .initialize({ notification, models })
        .execute(input)
    },
    async activateUser(_, { id }, ctx) {
      return useCases.activateUserUseCase.initialize(models).execute(id)
    },
    async deactivateUser(_, { id }, ctx) {
      return useCases.deactivateUserUseCase
        .initialize(models)
        .execute({ id, currentUser: ctx.user })
    },
    async editUserFromAdminPanel(_, { id, input }, ctx) {
      return useCases.editUserFromAdminPanelUseCase
        .initialize(models)
        .execute({ input, id, currentUser: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
