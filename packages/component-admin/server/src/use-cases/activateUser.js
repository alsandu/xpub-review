const initialize = ({ User }) => ({
  execute: async id => {
    const user = await User.find(id)
    if (user.isActive) return

    user.updateProperties({ isActive: true })
    await user.save()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
