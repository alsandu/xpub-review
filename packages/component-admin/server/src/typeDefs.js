module.exports = `
  input UserInput {
    givenNames: String
    surname: String
    email: String!
    aff: String
    title: AllowedTitle
    role: AllowedRole!
    country: String
  }

  input UserStatusInput {
    email: String!
    isActive: Boolean
  }

  input EditUserInput{
    givenNames: String
    surname: String
    aff: String
    title: AllowedTitle
    role: AllowedRole
    country: String
  }

  extend type Mutation{
    addUserFromAdminPanel(input: UserInput): Token
    editUserFromAdminPanel(id: ID! input: EditUserInput): Boolean  
    activateUser(id:ID!): Boolean  
    deactivateUser(id: ID!): Boolean  
  }

  extend type Query{
    getUsersForAdminPanel: [User]
  }

  enum AllowedRole {
    editorInChief
    handlingEditor
    admin
    user
  }

  enum AllowedTitle {
    mr
    mrs
    miss
    ms
    dr
    prof
  }
`
