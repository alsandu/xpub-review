const { get } = require('lodash')
const config = require('config')
const { services } = require('helper-service')

const Email = require('@pubsweet/component-email-templating')

const { getEmailCopyFroUserAddedByAdmin } = require('./emailCopy')

const unsubscribeSlug = config.get('unsubscribe.url')

const { name: journalName, staffEmail } = config.get('journal')

module.exports = {
  async notifyUserAddedByAdmin({ user, identity, role }) {
    const resetPath = config.get('invite-reset-password.url')
    const baseUrl = config.get('pubsweet-client.baseUrl')

    const { paragraph, ...bodyProps } = getEmailCopyFroUserAddedByAdmin({
      role,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: identity.email,
        name: identity.surname ? `${identity.surname}` : '',
      },
      content: {
        subject: 'Confirm your account',
        ctaLink: services.createUrl(baseUrl, resetPath, {
          userId: user.id,
          confirmationToken: user.confirmationToken,
          givenNames: get(identity, 'givenNames', ''),
          surname: get(identity, 'surname', ''),
          title: get(identity, 'title', ''),
          country: get(identity, 'country', ''),
          aff: get(identity, 'aff', ''),
          email: get(identity, 'email', ''),
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        signatureName: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
}
