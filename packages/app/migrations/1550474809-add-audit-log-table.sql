CREATE TABLE "audit_log" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "user_id" uuid REFERENCES "user",
  "manuscript_id" uuid REFERENCES manuscript,
  "action" text NOT NULL, -- invited HE, accepted invitation,
  "object_type" text NOT NULL, -- manuscript, user, review
  "object_id" uuid NOT NULL, -- target
  "journal_id" uuid REFERENCES journal
);