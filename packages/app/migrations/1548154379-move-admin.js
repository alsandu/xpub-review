const BaseModel = require('@pubsweet/base-model')
const models = require('@pubsweet/models')
const { v4 } = require('uuid')

class Entity extends BaseModel {
  static get tableName() {
    return 'entities'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'string', format: 'uuid' },
        data: { type: 'string', format: 'jsonb' },
      },
    }
  }
}

module.exports = {
  up: async () => {
    const entities = await Entity.all()
    const admin = entities.find(entity => entity.data.admin)
    if (!admin) {
      throw new Error('Admin not found!')
    }

    const adminUser = new models.User({
      defaultIdentity: 'local',
      isSubscribedToEmails: true,
      unsubscribeToken: v4(),
      agreeTc: true,
      isActive: true,
    })

    await adminUser.save()

    const identity = new models.Identity({
      userId: adminUser.id,
      type: 'local',
      isConfirmed: true,
      email: admin.data.email,
      givenNames: admin.data.firstName,
      surname: admin.data.lastName,
      title: admin.data.title,
      aff: admin.data.affiliation,
      country: admin.data.country,
      passwordHash: admin.data.passwordHash,
    })

    await adminUser.assignIdentity(identity)
    await adminUser.saveRecursively()

    const journals = await models.Journal.all()

    const team = new models.Team({
      role: 'admin',
      journalId: journals[0].id,
    })
    await team.save()

    const teamMember = team.addMember(adminUser, {
      userId: adminUser.id,
      teamId: team.id,
    })

    teamMember.updateProperties({ status: 'accepted' })
    await teamMember.save()
  },
}
