ALTER TABLE review
  RENAME COLUMN user_id TO team_member_id;

ALTER TABLE review
  ALTER COLUMN team_member_id TYPE uuid;

ALTER TABLE review
  DROP CONSTRAINT review_user_id_fkey;

ALTER TABLE review
  ADD CONSTRAINT review_team_member_id_fkey
  FOREIGN KEY (team_member_id) REFERENCES team_member;