const metadata = require('./metadata')
const decisions = require('./decisions')
const recommendations = require('./recommendations')
const sections = require('./sections')
const roles = require('./roles')
const issueTypes = require('./issues-types')
const manuscriptTypes = require('./manuscript-types')
const titles = require('./titles')
const wizard = require('./wizard')
const submission = require('./submission')
const statuses = require('./statuses')
const rolesAccess = require('./roles-access')
const activityLogEvents = require('./activity-log')
const activityLogObjectTypes = require('./activity-log-object-types')

module.exports = {
  statuses,
  metadata,
  recommendations,
  decisions,
  sections,
  roles,
  issueTypes,
  manuscriptTypes,
  titles,
  wizard,
  submission,
  rolesAccess,
  activityLogEvents,
  activityLogObjectTypes,
}
