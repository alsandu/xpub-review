module.exports = {
  questions: [
    {
      id: 'conflictsOfInterest',
      commentsFieldName: 'meta.conflicts.message',
      commentsSubtitle: '',
      subtitle: null,
      commentsPlaceholder: 'Please provide the conflicts of interest',
      commentsOn: 'yes',
      radioFieldName: 'meta.conflicts.hasConflicts',
      radioLabel: 'Do any authors have conflicts of interest to declare?',
      required: true,
    },
    {
      id: 'dataAvailability',
      commentsFieldName: 'meta.conflicts.dataAvailabilityMessage',
      commentsSubtitle:
        'For more information about the data availability statement, please ',
      subtitle: {
        link: 'https://www.hindawi.com/journals/bca/guidelines/',
        label: 'click here',
      },
      commentsPlaceholder:
        'Please provide a statement describing the availability of the underlying data related to your submission',
      commentsOn: 'no',
      radioFieldName: 'meta.conflicts.hasDataAvailability',
      radioLabel:
        'Have you included a data availability statement in your manuscript?',
      required: false,
    },
    {
      id: 'funding',
      commentsFieldName: 'meta.conflicts.fundingMessage',
      commentsSubtitle: '',
      commentsPlaceholder:
        'Please provide a statement describing how the research and publication of your article is funded',
      subtitle: null,
      commentsOn: 'no',
      radioFieldName: 'meta.conflicts.hasFunding',
      radioLabel: 'Have you provided a funding statement in your manuscript?',
      required: false,
    },
  ],
  links: {
    authorGuidelines: 'https://www.hindawi.com/journals/bca/guidelines/',
    articleProcessing: 'https://www.hindawi.com/journals/bca/apc/',
  },
}
