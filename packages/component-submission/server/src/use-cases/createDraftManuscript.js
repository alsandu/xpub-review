const generateCustomId = () =>
  Date.now()
    .toString()
    .slice(-7)

const initialize = ({ Manuscript, Team, TeamMember, User, Journal }) => ({
  async execute({ journalId }, userId) {
    if (!journalId) {
      const journals = await Journal.all()
      journalId = journals[0].id
    }

    const author = await User.find(userId, 'identities')
    const teamMemberships = await TeamMember.findBy({ userId }, 'team')
    const userBelongsToAdminTeam = teamMemberships.some(
      t => t.team.role === 'admin',
    )

    const manuscript = new Manuscript({
      journalId,
      customId: generateCustomId(),
    })

    const authorTeam = new Team({ role: 'author', journalId })
    manuscript.assignTeam(authorTeam)

    if (!userBelongsToAdminTeam) {
      authorTeam.addMember(author, {
        isSubmitting: true,
        isCorresponding: true,
      })
    }

    await manuscript.saveRecursively()

    return manuscript.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
