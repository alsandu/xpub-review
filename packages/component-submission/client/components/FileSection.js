import React from 'react'
import { isNumber } from 'lodash'
import { FieldArray } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { FilePicker, Spinner } from '@pubsweet/ui'
import {
  withNativeFileDrop,
  withFileSectionDrop,
  getSupportedFileFormats,
} from 'component-files/client'
import { compose, setDisplayName, withHandlers, withProps } from 'recompose'
import {
  Row,
  File,
  Text,
  Item,
  Icon,
  Label,
  ActionLink,
  DragHandle,
  SortableList,
  withFetching,
  radiusHelpers,
} from 'component-hindawi-ui'

export const Files = ({
  files,
  title,
  remove,
  isLast,
  isFirst,
  compact,
  required,
  listName,
  onDelete,
  onUpload,
  maxFiles,
  moveFile,
  onPreview,
  isFetching,
  isFileOver,
  fetchingError,
  isFileItemOver,
  connectFileDrop,
  canDropFileItem,
  supportedFormats,
  connectDropTarget,
  allowedFileExtensions,
}) => (
  <Root
    compact={compact}
    isFileItemOver={isFileOver || (isFileItemOver && canDropFileItem)}
    isFirst={isFirst}
    isLast={isLast}
    ref={instance => {
      connectFileDrop(instance)
      connectDropTarget(instance)
    }}
  >
    <Row alignItems="center" mb={1}>
      <Item alignItems="baseline">
        <Label required={required}>{title}</Label>
        {isFetching ? (
          <Spinner />
        ) : (
          <FilePicker
            allowedFileExtensions={allowedFileExtensions}
            disabled={files.length >= maxFiles}
            onUpload={onUpload}
          >
            <ActionLink
              data-test-id={`add-file-${listName}`}
              disabled={files.length >= maxFiles}
              fontSize="12px"
              fontWeight="bold"
              size="small"
            >
              <Icon
                bold
                color="colorWarning"
                fontSize="10px"
                icon="expand"
                ml={1}
                mr={1 / 2}
                mt={1}
              />
              UPLOAD FILE
            </ActionLink>
          </FilePicker>
        )}
      </Item>
      {supportedFormats && (
        <Item justify="flex-end">
          <Text fontSize="13px" mt={1} secondary>
            Supported file formats: {supportedFormats}
          </Text>
        </Item>
      )}
    </Row>

    <SortableList
      beginDragProps={[
        'id',
        'index',
        'mimeType',
        'originalName',
        'listName',
        'remove',
      ]}
      dragHandle={DragHandle}
      hasDelete
      items={files}
      listItem={File}
      listName={listName}
      mb={1}
      moveItem={moveFile}
      onDelete={onDelete}
      onPreview={onPreview}
      remove={remove}
      shadow
    />

    {fetchingError && (
      <Row mb={2}>
        <Text error>{fetchingError}</Text>
      </Row>
    )}
  </Root>
)

export const EnhancedFiles = compose(
  withFetching,
  setDisplayName('FileSection'),
  withProps(({ allowedFileExtensions = [], name, values }) => ({
    supportedFormats: getSupportedFileFormats(allowedFileExtensions),
  })),
  withHandlers({
    moveFile: ({ move }) => (startIndex, endIndex) => {
      if (isNumber(startIndex) && isNumber(endIndex)) {
        move(startIndex, endIndex)
      }
    },
    onDelete: ({
      remove,
      setError,
      clearError,
      setFetching,
      onDeleteFile,
      files = [],
    }) => file => {
      clearError()
      onDeleteFile(file, {
        remove,
        setError,
        setFetching,
        index: files.findIndex(f => f.id === file.id),
      })
    },
    onUpload: ({
      push,
      listName,
      setError,
      clearError,
      setFetching,
      onUploadFile,
    }) => file => {
      clearError()
      if (typeof onUploadFile === 'function') {
        onUploadFile(file, { type: listName, push, setError, setFetching })
      }
    },
  }),
  withProps(({ onUpload }) => ({ onNativeFileDrop: onUpload })),
  withNativeFileDrop,
  withFileSectionDrop,
)(Files)

const FileSection = ({ listName, ...rest }) => (
  <FieldArray name={`files.${listName}`}>
    {({ push, remove, move }) => (
      <EnhancedFiles
        listName={listName}
        move={move}
        push={push}
        remove={remove}
        {...rest}
      />
    )}
  </FieldArray>
)

export default FileSection

// #region styles
const Root = styled.div`
  background: ${props =>
    props.isFileItemOver ? th('colorFurniture') : th('colorBackground')};
  min-height: calc(${th('gridUnit')} * ${({ compact }) => (compact ? 15 : 20)});
  padding: 0 ${th('gridUnit')};
  width: 100%;

  ${radiusHelpers};
  border-bottom: ${props => (!props.isLast ? '1px dashed #dbdbdb' : 'none')};
`
// #endregion
