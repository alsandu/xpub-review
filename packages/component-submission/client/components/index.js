import WizardStepOne from './WizardStepOne'
import WizardStepTwo from './WizardStepTwo'
import WizardStepThree from './WizardStepThree'

export { default as WizardFiles } from './WizardFiles'
export { default as WizardAuthors } from './WizardAuthors'
export { default as WizardButtons } from './WizardButtons'
export { default as AutosaveIndicator } from './AutosaveIndicator'
export { default as SubmissionStatement } from './SubmissionStatement'

export const wizardSteps = [
  {
    stepTitle: '1. Pre-submission Checklist',
    component: WizardStepOne,
  },
  {
    stepTitle: '2. Manuscript & Author Details',
    component: WizardStepTwo,
  },
  {
    stepTitle: '3. Files Upload',
    component: WizardStepThree,
  },
]
