const Chance = require('chance')
const { pick, assign } = require('lodash')

const chance = new Chance()
let teamMembers = []

function TeamMember(props) {
  return {
    id: chance.guid(),
    position: props.position || null,
    isSubmitting: props.isSubmitting || false,
    isCorresponding: props.isCorresponding || false,
    status: props.status || null,
    userId: props.userId || null,
    teamId: props.teamId || null,
    reviewerNumber: props.reviewerNumber || null,
    async save() {
      const existingTeamMember = teamMembers.find(m => m.id === this.id)
      if (existingTeamMember) {
        assign(existingTeamMember, this)
      } else {
        teamMembers.push(this)
      }
      return Promise.resolve(this)
    },
    async linkUser(user) {
      this.user = user
      if (!this.alias) {
        const defaultIdentity = user.getDefaultIdentity()
        this.alias = pick(defaultIdentity, [
          'surname',
          'title',
          'givenNames',
          'email',
          'aff',
          'country',
        ])
      }
    },
    async saveRecursively() {
      this.save()
    },
    toDTO() {
      return {
        ...this,
        user: this.user ? this.user.toDTO() : undefined,
        alias: {
          ...this.alias,
          name: {
            surname: this.alias.surname,
            givenNames: this.alias.givenNames,
          },
        },
      }
    },
    updateProperties(properties) {
      assign(this, properties)
      return this
    },
    async delete() {
      if (this.team) {
        this.team.members = this.team.members.filter(
          member => member.id !== this.id,
        )
      }
      teamMembers = teamMembers.filter(member => member.id !== this.id)
    },
  }
}

TeamMember.Statuses = {
  pending: 'pending',
  accepted: 'accepted',
  declined: 'declined',
  submitted: 'submitted',
}

const generateTeamMember = properties => {
  const teamMember = new TeamMember(properties || {})

  teamMembers.push(teamMember)

  return teamMember
}

const findTeamMember = (teamId, userId) => {
  teamMembers.find(
    teamMember =>
      teamMember.user_id === userId && teamMember.team_id === teamId,
  )
}

module.exports = { teamMembers, generateTeamMember, findTeamMember, TeamMember }
