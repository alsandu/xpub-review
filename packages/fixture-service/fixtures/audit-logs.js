const Chance = require('chance')

const chance = new Chance()
const logs = []

function AuditLog(props) {
  return {
    id: chance.guid(),
    created: props.create || Date.now(),
    updated: props.updated || Date.now(),
    userId: props.userId,
    manuscriptId: props.manuscriptId,
    action: props.action || chance.word(),
    objectType: props.objectType,
    objectId: props.objectId,
    journalId: props.journalId || chance.word(),
    async save() {
      logs.push(this)
      return Promise.resolve(this)
    },
    async linkUser(user) {
      this.user = user
      await user.saveRecursively()
    },
    toDTO() {
      return {
        ...this,
        user: this.user ? this.user.toDTO() : undefined,
      }
    },
  }
}

const generateAuditLog = properties => {
  const auditLog = new AuditLog(properties)
  logs.push(auditLog)
  return auditLog
}

module.exports = { logs, AuditLog, generateAuditLog }
