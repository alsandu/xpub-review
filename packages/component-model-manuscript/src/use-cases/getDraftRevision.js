const initialize = ({ Team, Manuscript, User }) => ({
  execute: async ({ submissionId, userId }) => {
    const draftRevision = await Manuscript.findOneBy({
      queryObject: { submissionId, status: Manuscript.Statuses.draft },
      eagerLoadRelations: [
        'files',
        'teams.members.user.identities',
        'reviews.comments.files',
      ],
    })

    if (!draftRevision) {
      return
    }

    const user = await User.find(userId, 'teamMemberships.[team]')

    const submittingAuthorMember = user.teamMemberships.find(
      member =>
        member.team.manuscriptId === draftRevision.id &&
        member.isSubmitting === true &&
        member.team.role === Team.Role.author,
    )

    if (submittingAuthorMember) {
      draftRevision.setComment()
      return draftRevision.toDTO()
    }
  },
})

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
