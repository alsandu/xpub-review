const config = require('config')
const { get, chain } = require('lodash')

const GLOBAL_ROLES = config.get('globalRoles')

const initialize = ({ Journal, Manuscript, User, Team, TeamMember }) => ({
  execute: async ({ submissionId, userId }) => {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      excludedStatus: Manuscript.Statuses.draft,
      orderByField: 'version',
      order: 'desc',
      eagerLoadRelations: [
        'files',
        'teams.[members.[user.[identities]]]',
        'journal',
      ],
    })

    const requestingHeUser = chain(manuscripts)
      .flatMap(manuscript => manuscript.teams)
      .filter(team => team.role === Team.Role.handlingEditor)
      .flatMap(team => team.members)
      .find(member => member.userId === userId)
      .value()

    if (
      requestingHeUser &&
      get(requestingHeUser, 'status') === TeamMember.Statuses.removed
    ) {
      throw new Error('Operation not permitted!')
    }

    const manuscriptIds = manuscripts.map(m => m.id)

    const user = await User.find(userId, 'teamMemberships.[team]')
    const globalRole = user.getGlobalRole()

    const userRoleOnManuscripts = user.teamMemberships
      .filter(({ team }) => manuscriptIds.includes(team.manuscriptId))
      .reduce(
        (acc, tm) => ({ ...acc, [tm.team.manuscriptId]: tm.team.role }),
        {},
      )

    const journal = await Journal.find(
      manuscripts[0].journalId,
      'teams.members',
    )
    const editorInChief = await journal.getEditorInChief()

    manuscripts.forEach(m => {
      m.editorInChief = editorInChief

      m.role = GLOBAL_ROLES.includes(globalRole)
        ? globalRole
        : userRoleOnManuscripts[m.id]
    })
    const excludedStatuses = [
      TeamMember.Statuses.declined,
      TeamMember.Statuses.expired,
      TeamMember.Statuses.removed,
    ]

    const shouldFilterManuscripts = manuscript => {
      if (GLOBAL_ROLES.includes(manuscript.role)) return true
      return !excludedStatuses.includes(
        manuscript.teams
          .find(t => t.role === manuscript.role)
          .members.find(member => member.userId === userId).status,
      )
    }
    const visibleManuscripts = manuscripts.filter(
      m => m.role && shouldFilterManuscripts(m),
    )

    return visibleManuscripts.map(m => m.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
