process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const chance = new Chance()

const { getManuscriptVersionsUseCase } = require('../src/use-cases')

describe('Get Manuscript Versions Use Case', () => {
  it('return manuscript order by version desc', async () => {
    const journal = fixtures.journals[0]

    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    const submissionId = chance.guid()

    const firstVersion = fixtures.generateManuscript({
      version: 1,
      submissionId,
      status: 'olderVersion',
      journal: journal.id,
    })

    firstVersion.journalId = journal.id
    const secondVersion = fixtures.generateManuscript({
      version: 2,
      submissionId,
      status: 'olderVersion',
      journal: journal.id,
    })
    secondVersion.journalId = journal.id
    const thirdVersion = fixtures.generateManuscript({
      version: 3,
      submissionId,
      status: 'heAssigned',
      journal: journal.id,
    })
    thirdVersion.journalId = journal.id

    const authorMember = await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })

    const author = fixtures.users.find(user => user.id === authorMember.userId)

    await dataService.addUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      user: author,
      role: 'author',
    })

    await dataService.addUserOnManuscript({
      manuscript: thirdVersion,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      user: author,
      role: 'author',
    })
    const mockedModels = models.build(fixtures)

    const manuscriptVersions = await getManuscriptVersionsUseCase
      .initialize(mockedModels)
      .execute({ submissionId, userId: authorMember.userId })

    expect(manuscriptVersions[0].version).toEqual(3)
    expect(manuscriptVersions[1].version).toEqual(2)
    expect(manuscriptVersions[2].version).toEqual(1)
  })
})
