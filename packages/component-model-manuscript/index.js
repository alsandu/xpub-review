const fs = require('fs')
const path = require('path')

const Manuscript = require('./src/entities/manuscript')
const resolvers = require('./src/resolvers')

module.exports = {
  model: Manuscript,
  modelName: 'Manuscript',
  resolvers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/src/typeDefs.graphqls'),
    'utf8',
  ),
}
