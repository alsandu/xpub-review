const Review = require('./src/review')

module.exports = {
  model: Review,
  modelName: 'Review',
}
