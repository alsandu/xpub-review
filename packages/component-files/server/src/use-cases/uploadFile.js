const uuid = require('uuid')

const initialize = ({
  logEvent,
  s3Service,
  models: { Comment, File, Manuscript },
}) => {
  const manuscriptTypes = [
    File.Types.manuscript,
    File.Types.supplementary,
    File.Types.coverLetter,
  ]
  const commentTypes = [
    File.Types.reviewComment,
    File.Types.responseToReviewers,
  ]
  return {
    execute: async ({
      params: { entityId, fileInput, file: fileData },
      reqUserId,
    }) => {
      if (fileInput.size > 50000000) {
        return new Error('File must not be bigger than 50MB.')
      }

      const { stream, filename, mimetype } = await fileData
      const isCommentFile = commentTypes.includes(fileInput.type)
      const isManuscriptFile = manuscriptTypes.includes(fileInput.type)
      const key = `${entityId}/${uuid.v4()}`

      await s3Service.upload({
        key,
        stream,
        mimetype,
        metadata: {
          filename,
          type: fileInput.type,
        },
      })

      const file = new File({
        manuscriptId: isManuscriptFile ? entityId : null,
        commentId: isCommentFile ? entityId : null,
        size: fileInput.size,
        fileName: key,
        mimeType: mimetype,
        originalName: filename,
        type: File.Types[fileInput.type],
      })
      await file.save()

      if (isManuscriptFile) {
        const manuscript = await Manuscript.find(entityId, 'files')

        manuscript.assignFile(file)

        await manuscript.save()
        if (manuscript.status !== 'draft') {
          logEvent({
            userId: reqUserId,
            manuscriptId: entityId,
            action: logEvent.actions.file_added,
            objectType: logEvent.objectType.file,
            objectId: file.id,
          })
        }
      }

      if (isCommentFile) {
        const comment = await Comment.find(entityId, 'files')
        comment.assignFile(file)
        await comment.save()
      }

      return file
    },
  }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
