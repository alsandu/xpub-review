const initialize = ({ models: { File }, s3Service }) => ({
  execute: async fileId => {
    const file = await File.find(fileId)

    return s3Service.getSignedUrl(file.fileName)
  },
})

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
