const { get } = require('lodash')
const nodeArchiver = require('archiver')

const zipFiles = ({ s3, models: { File }, archiver = nodeArchiver }) => async (
  req,
  res,
) => {
  const { manuscriptId } = req.params
  const manuscriptFileTypes = [
    File.Types.manuscript,
    File.Types.coverLetter,
    File.Types.supplementary,
  ]

  const manuscriptFiles = await File.findBy({ manuscriptId })

  const filteredFiles = manuscriptFiles.filter(file =>
    manuscriptFileTypes.includes(file.type),
  )
  const archive = archiver('zip')
  archive.pipe(res)

  filteredFiles
    .map(file => ({ stream: s3.getObjectStream(file.fileName), file }))
    .forEach(({ stream, file }) => {
      archive.append(stream, {
        name: `${get(file, 'type', 'supplementary')}/${get(
          file,
          'originalName',
        )}`,
      })
    })

  archive.finalize()
}

module.exports = zipFiles
