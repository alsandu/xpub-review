const fs = require('fs')
const path = require('path')

const { resolvers, server } = require('./server')

module.exports = {
  server,
  resolvers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/server/src/typeDefs.graphqls'),
    'utf8',
  ),
}
