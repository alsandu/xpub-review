import { get } from 'lodash'
import { H2 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { marginHelper, paddingHelper } from '../'

const width = props => css`
  width: calc(${th('gridUnit')} * ${get(props, 'width', 50)});
`

export default styled.div.attrs(props => ({
  pt: get(props, 'pt', 2),
  pr: get(props, 'pr', 2),
  pb: get(props, 'pb', 2),
  pl: get(props, 'pl', 2),
}))`
  background-color: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;

  position: ${props => get(props, 'position', 'initial')};

  ${width};
  ${marginHelper};
  ${paddingHelper};

  ${H2} {
    text-align: center;
  }
`
