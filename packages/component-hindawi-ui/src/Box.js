import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { paddingHelper, marginHelper } from './styledHelpers'

const Box = styled.div`
  background-color: ${th('box.backgroundColor')};
  border: ${th('box.border')};
  border-radius: ${th('box.borderRadius')};

  ${paddingHelper}
  ${marginHelper}
`

export default Box
