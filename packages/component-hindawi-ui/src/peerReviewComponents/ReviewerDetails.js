import React, { Fragment } from 'react'
import { get } from 'lodash'
import { H3 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import {
  Tag,
  Tabs,
  Item,
  ContextualBox,
  ReviewerBreakdown,
  marginHelper,
} from '../../'

const ReviewerDetails = ({
  mt,
  children,
  isVisible,
  tabButtons,
  highlight,
  startExpanded,
  reviewers = [],
  numberOfSubmittedReports,
}) => {
  numberOfSubmittedReports = reviewers.filter(
    reviewer => get(reviewer, 'status') === 'submitted',
  ).length

  let expandedTab
  if (numberOfSubmittedReports && tabButtons.length === 3) {
    expandedTab = 2
  } else if (numberOfSubmittedReports) {
    expandedTab = 1
  } else expandedTab = 0

  return isVisible ? (
    <ContextualBox
      highlight={highlight}
      label="Reviewer Details & Reports"
      mt={mt}
      reviewers={reviewers}
      rightChildren={ReviewerBreakdown}
      startExpanded={startExpanded}
    >
      <Tabs selectedTab={expandedTab}>
        {({ selectedTab, changeTab }) => (
          <Fragment>
            <TabsHeader>
              {tabButtons.map((tab, index) => (
                <TabButton
                  key={tab}
                  ml={1}
                  mr={1}
                  onClick={() => changeTab(index)}
                  selected={selectedTab === index}
                >
                  {renderTabButton(tab, numberOfSubmittedReports)}
                </TabButton>
              ))}
            </TabsHeader>
            {React.Children.toArray(children)[selectedTab]}
          </Fragment>
        )}
      </Tabs>
    </ContextualBox>
  ) : null
}

export default ReviewerDetails

const renderTabButton = (tab, numberOfSubmittedReports) => {
  if (typeof tab === 'function') {
    return React.createElement(tab)
  }
  if (typeof tab === 'string' && tab === 'Reviewer Reports') {
    return (
      <Item>
        <H3>{tab}</H3>
        <Tag ml={1 / 2}>{numberOfSubmittedReports}</Tag>
      </Item>
    )
  }
  if (typeof tab === 'string') {
    return <H3>{tab}</H3>
  }
  return tab
}

// #region
const TabButton = styled.div`
  align-items: center;
  border-bottom: ${props =>
    props.selected
      ? `4px solid ${props.theme.colorFurnitureHue}`
      : '4px solid transparent'};
  box-sizing: border-box;
  cursor: pointer;
  display: flex;
  justify-content: center;
  height: calc(${th('gridUnit')} * 5);
  padding: 0 ${th('gridUnit')};
  padding-top: ${th('gridUnit')};

  ${marginHelper};
`
const TabsHeader = styled.div`
  align-items: center;
  border-bottom: 1px solid ${th('colorFurnitureHue')};
  background-color: ${th('colorBackgroundHue2')};
  box-sizing: border-box;
  display: flex;
  justify-content: flex-start;

  padding: 0 calc(${th('gridUnit')} * 3);
`
// #endregion
