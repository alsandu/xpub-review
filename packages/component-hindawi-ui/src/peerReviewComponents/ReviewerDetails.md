The invite reviewers contextual box.

```js
const tabButtons = [
  'First tab',
  <div>Second tab</div>,
  () => <span>Third tab</span>,
]

const reviewerReports = [
  {
    id: '71effdc0-ccb1-4ea9-9422-dcc9f8713347',
    reviewerNumber: 10,
    comments: [
      {
        id: 'fdfdfd0-ccb1-4ea9-9422-dcc9f8713347',
        content: "I think that's pretty good.",
        type: 'public',
        file: {
          id: 'file2',
          originalName: 'file2.docx',
          mimeType: 'application/pdf',
          size: 51312,
        },
      },
      {
        id: 'fdeffdc0-ccb1-4ea9-9422-dcc9f8713347',
        content: "Trust me it's pretty good.",
        type: 'private',
        file: [],
      },
    ],
    created: 1538053564396,
    member: {
      alias: { name: { surname: 'Ursachi', givenNames: 'Anca Ioana' } },
      role: 'reviewer',
      user: { id: 'fdfdfd0-ccb1-4ea9-9422-dcc9f8713347' },
    },
    open: true,
    recommendation: 'publish',
    submitted: 1538053564396,
  },
  {
    id: '71ef45c0-ccb1-4ea9-9422-dcc9f8713347',
    reviewerNumber: 10,
    comments: [
      {
        id: 'fdfdfd0-ccb1-4ea9-9422-dcc9f8713347',
        content: "I think that's pretty good.",
        type: 'public',
        file: {
          id: 'file2',
          originalName: 'file2.docx',
          mimeType: 'application/pdf',
          size: 51312,
        },
      },
      {
        id: 'fdeffdc0-ccb1-4ea9-9422-dcc9f8713347',
        content: "Trust me it's pretty good.",
        type: 'private',
        file: [],
      },
    ],
    created: 1538053564396,
    member: {
      alias: { name: { surname: 'Ursachi', givenNames: 'Anca Ioana' } },
      role: 'reviewer',
      user: { id: 'fdfdfd0-ccb1-4ea9-9422-dcc9f8713347' },
    },
    open: true,
    recommendation: 'publish',
    submitted: 1538053564396,
  },
]

;<ReviewerDetails
  isVisible
  tabButtons={tabButtons}
  reviewerReports={reviewerReports}
>
  <span>one</span>
  <span>two</span>
  <span>three</span>
</ReviewerDetails>
```
