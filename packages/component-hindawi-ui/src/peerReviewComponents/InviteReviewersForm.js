import React from 'react'
import { Formik } from 'formik'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withHandlers } from 'recompose'
import { Button, H3, TextField, Spinner } from '@pubsweet/ui'

import {
  Row,
  Item,
  Label,
  Text,
  MenuCountry,
  ItemOverrideAlert,
  ValidatedFormField,
  validators,
  withFetching,
  trimFormStringValues,
} from '../..'

const InviteReviewersForm = ({ onInvite, isFetching, fetchingError }) => (
  <Formik onSubmit={onInvite}>
    {({ handleSubmit, resetForm, values }) => (
      <Root>
        <Row justify="space-between" mb={1}>
          <H3>Invite Reviewer</H3>
          {fetchingError && (
            <Item justify="flex-end">
              <Text error>An error has occurred on your request!</Text>
            </Item>
          )}
          {isFetching ? (
            <Item justify="flex-end">
              <Spinner />
            </Item>
          ) : (
            <Item justify="flex-end">
              <Button
                data-type-id="button-invite-reviewer-invite"
                ml={2}
                onClick={() => resetForm({})}
                secondary
                size="small"
              >
                CLEAR
              </Button>

              <Button
                data-type-id="button-invite-reviewer-invite"
                ml={2}
                onClick={handleSubmit}
                primary
                size="small"
              >
                SEND
              </Button>
            </Item>
          )}
        </Row>
        <Row>
          <Item mr={2} vertical>
            <Label required>Email</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="reviewer-email"
              inline
              name="email"
              validate={[validators.required, validators.emailValidator]}
            />
          </Item>

          <Item mr={2} vertical>
            <Label required>First Name</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="reviewer-givenNames"
              inline
              name="givenNames"
              validate={[validators.required]}
            />
          </Item>

          <Item mr={2} vertical>
            <Label required>Last Name</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="reviewer-surname"
              inline
              name="surname"
              validate={[validators.required]}
            />
          </Item>
          <Item mr={2} vertical>
            <Label>Affiliation</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="reviewer-aff"
              inline
              name="aff"
            />
          </Item>

          <ItemOverrideAlert vertical>
            <Label>Country</Label>
            <ValidatedFormField
              component={MenuCountry}
              data-test-id="reviewer-country"
              name="country"
            />
          </ItemOverrideAlert>
        </Row>
      </Root>
    )}
  </Formik>
)

InviteReviewersForm.propTypes = {
  /** Callback function fired after confirming a reviewer invitation.
   * @param {Reviewer} reviewer
   * @param {object} props
   */
  onInvite: PropTypes.func, // eslint-disable-line
}

InviteReviewersForm.defaultProps = {
  onInvite: () => {},
}

export default compose(
  withFetching,
  withHandlers({
    onInvite: ({ onInvite }) => (values, formProps) => {
      onInvite(trimFormStringValues(values), formProps)
    },
  }),
)(InviteReviewersForm)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue3')};
  padding: calc(${th('gridUnit')} * 2);
  padding-bottom: ${th('gridUnit')};
`
// #endregion
