import React from 'react'
import { withProps } from 'recompose'
import { get } from 'lodash'
import { Row, Text } from '../../'

const ReviewerBreakdown = ({ accepted, declined, reviewers, submitted }) =>
  reviewers && reviewers.length ? (
    <Row alignItems="baseline" fitContent justify="flex-end" mr={1}>
      <Text customId mr={1 / 2}>
        {reviewers.length}
      </Text>
      <Text mr={1 / 2} secondary>
        invited,
      </Text>

      <Text customId mr={1 / 2}>
        {accepted}
      </Text>
      <Text mr={1 / 2} secondary>
        agreed,
      </Text>

      <Text customId mr={1 / 2}>
        {declined}
      </Text>
      <Text mr={1 / 2} secondary>
        declined,
      </Text>

      <Text customId mr={1 / 2}>
        {submitted}
      </Text>
      <Text mr={1 / 2} secondary>
        submitted
      </Text>
    </Row>
  ) : (
    <Row alignItems="baseline" fitContent justify="flex-end" mr={1}>
      <Text secondary>0 invited</Text>
    </Row>
  )

export default withProps(({ reviewers = [] }) => ({
  accepted: reviewers
    ? reviewers.filter(reviewer => get(reviewer, 'status') === 'accepted')
        .length +
      reviewers.filter(reviewer => get(reviewer, 'status') === 'submitted')
        .length
    : [],
  declined: reviewers
    ? reviewers.filter(reviewer => get(reviewer, 'status') === 'declined')
        .length
    : [],
  submitted: reviewers
    ? reviewers.filter(reviewer => get(reviewer, 'status') === 'submitted')
        .length
    : [],
}))(ReviewerBreakdown)
