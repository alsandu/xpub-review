EIC Decision box

```js
const mockOptions = [
  {
    label: 'Request revision',
    value: 'revision',
  },
  {
    label: 'Reject',
    value: 'reject',
  },
]
;<EICDecision
  startExpanded
  isVisible={true}
  options={mockOptions}
  onSubmit={({ decision, message }) => console.log(decision, message)}
/>
```
