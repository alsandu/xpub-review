import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { marginHelper, paddingHelper } from './styledHelpers'

const textHelper = props => {
  if (get(props, 'secondary')) {
    return css`
      color: ${th('colorSecondary')};
    `
  }
  if (get(props, 'error')) {
    return css`
      color: ${th('colorError')};
    `
  }

  if (get(props, 'emptyState')) {
    return css`
      color: ${th('colorSecondary')};
      font-size: ${th('fontSizeBaseMedium')};
      line-height: ${th('lineHeightHeading3')};
    `
  }

  if (get(props, 'customId')) {
    return css`
      color: ${th('colorPrimary')};
      font-weight: 700;
      font-size: ${th('fontSizeBaseMedium')};
    `
  }

  if (get(props, 'labelLine')) {
    return css`
      color: ${th('colorFurnitureHue')};
      display: flex;
      &:after {
        display: block;
        content: ' ';
        border-bottom: 1px solid ${th('colorBorder')};
        flex: 1 1 auto;
        margin-left: calc(${th('gridUnit')} * 2);
      }
    `
  }

  if (get(props, 'journal')) {
    return css`
      color: ${th('colorSecondary')};
      &:before {
        content: '•';
        padding-right: ${th('gridUnit')};
      }
    `
  }

  return css`
    color: ${th('colorText')};
    line-height: ${get(props, 'lineHeight', 1)};
  `
}

const fontSize = props => {
  if (get(props, 'small')) {
    return css`
      font-size: ${th('fontSizeBaseSmall')};
    `
  }
  if (get(props, 'medium')) {
    return css`
      font-size: ${th('fontSizeBaseMedium')};
    `
  }
  return css`
    font-size: ${th('fontSizeBase')};
  `
}

const ellipsis = props => {
  if (props.ellipsis) {
    return css`
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
    `
  }

  return css`
    white-space: ${props => get(props, 'whiteSpace', 'initial')};
  `
}

const StyledText = styled.span`
  align-items: center;
  display: ${props => get(props, 'display', 'inline-block')};
  font-family: ${th('defaultFont')};
  font-style: ${props => get(props, 'fontStyle', 'normal')};
  font-weight: ${props => get(props, 'fontWeight', 'normal')};
  height: ${props => get(props, 'height', 'inherit')};
  text-align: ${props => get(props, 'align', 'start')};
  opacity: ${props => (props.opacity ? props.opacity : '1')};

  ${fontSize};
  ${ellipsis};
  ${textHelper};
  ${marginHelper};
  ${paddingHelper};
`

const Bullet = styled.span`
  color: ${th('colorWarning')};
  font-size: ${th('fontSizeHeading2')};
  line-height: 1;
  margin-right: ${th('gridUnit')};
  vertical-align: middle;
`

const Text = ({ bullet, children, ...rest }) =>
  bullet ? (
    <StyledText {...rest}>
      <Bullet>{'\u2022'}</Bullet>
      {children}
    </StyledText>
  ) : (
    <StyledText {...rest}>{children}</StyledText>
  )

Text.propTypes = {
  /** Default color for non-primary actions. */
  secondary: PropTypes.bool,
  /** Default color for error actions. */
  error: PropTypes.bool,
  /** Default style for the customId text. */
  customId: PropTypes.bool,
  /** Default style for text used as a label Line. */
  labelLine: PropTypes.bool,
  /** Default style used for journal text. */
  journal: PropTypes.bool,
  /** Default style used for small text. */
  small: PropTypes.bool,
}

Text.defaultProps = {
  secondary: false,
  error: false,
  customId: false,
  labelLine: false,
  journal: false,
  small: false,
}

export default Text
