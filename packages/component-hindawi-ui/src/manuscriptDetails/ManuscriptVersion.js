import React from 'react'
import { chain, get } from 'lodash'
import PropTypes from 'prop-types'
import { compose, withHandlers, withProps } from 'recompose'

import { Menu } from '../../'

const ManuscriptVersion = ({ versions, onChange, value }) =>
  versions.length > 0 && (
    <Menu
      inline
      onChange={onChange}
      options={versions}
      placeholder="Please select"
      value={value}
      width={15}
    />
  )

ManuscriptVersion.propTypes = {
  /** Object with versions of manuscript. */
  versions: PropTypes.array, //eslint-disable-line
}

ManuscriptVersion.defaultProps = {
  versions: [],
}
export default compose(
  withProps(({ match, versions = [] }) => ({
    value: chain(versions)
      .find(({ value }) => value === get(match, 'params.manuscriptId'))
      .get('value', '')
      .value(),
  })),
  withHandlers({
    onChange: ({ history, match }) => manuscriptId => {
      history.replace(
        `/details/${get(match, 'params.submissionId')}/${manuscriptId}`,
      )
    },
  }),
)(ManuscriptVersion)
