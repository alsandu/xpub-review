import { withProps } from 'recompose'
import { get } from 'lodash'

export default withProps(({ role, handlingEditor }) => {
  const heStatus = get(handlingEditor, 'status', '')
  let handlingEditorVisibleStatus = 'Unassigned'
  if (heStatus === 'accepted') {
    handlingEditorVisibleStatus = `${get(
      handlingEditor,
      'alias.name.surname',
      '',
    )} ${get(handlingEditor, 'alias.name.givenNames', '')}`
  } else if (role === 'author' && heStatus === 'pending') {
    handlingEditorVisibleStatus = 'Assigned'
  } else if (heStatus === 'pending') {
    handlingEditorVisibleStatus = 'Invited'
  }
  return {
    handlingEditorVisibleStatus,
  }
})
