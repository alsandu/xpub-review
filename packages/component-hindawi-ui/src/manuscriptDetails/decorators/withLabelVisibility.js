import { withProps } from 'recompose'

export default withProps(
  ({ role, heStatus, isLatestVersion, manuscriptStatus }) => ({
    canSeeLabelAuthorReviewer:
      ['pending', 'accepted', ''].includes(heStatus) &&
      !['admin', 'editorInChief'].includes(role),

    canSeeLabelOldVersionsAdminOrEiC:
      ['accepted', '', 'pending'].includes(heStatus) &&
      !isLatestVersion &&
      ['admin', 'editorInChief'].includes(role),

    canSeeLabelUnassigned:
      heStatus === '' &&
      ['admin', 'editorInChief'].includes(role) &&
      ['technicalChecks', 'revisionRequested'].includes(manuscriptStatus),

    canSeeInvite:
      isLatestVersion &&
      heStatus === '' &&
      ['admin', 'editorInChief'].includes(role) &&
      !['technicalChecks', 'revisionRequested'].includes(manuscriptStatus),

    canSeeRevoke:
      isLatestVersion &&
      heStatus === 'accepted' &&
      ['admin', 'editorInChief'].includes(role) &&
      manuscriptStatus !== 'technicalChecks',

    canSeeResendRemove:
      heStatus === 'pending' &&
      ['admin', 'editorInChief'].includes(role) &&
      isLatestVersion,
  }),
)
