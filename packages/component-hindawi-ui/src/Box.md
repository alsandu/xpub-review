Box component used to wrap various content (e.g. inside ContextualBox)

```js
<Box pl={2} pt={2} pr={2} pb={2}>
  Some random content
</Box>
```
