import React from 'react'
import { H4 } from '@pubsweet/ui'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { heightHelper, marginHelper } from '../'

const Label = ({ children, required, height, ...rest }) => (
  <Root height={height} {...rest}>
    <H4>{children}</H4>
    {required && <Required height={height}>*</Required>}
  </Root>
)

Label.propTypes = {
  /** If true the label is required. */
  required: PropTypes.bool,
  height: PropTypes.number,
}

Label.defaultProps = {
  required: false,
  height: 2,
}
export default Label

// #region styles
const Required = styled.span`
  color: ${th('colorError')};
  margin-left: calc(${th('gridUnit')} / 4);

  ${heightHelper};
`

const Root = styled.div`
  align-items: center;
  display: flex;

  ${heightHelper};
  ${marginHelper};
`
// #endregion
