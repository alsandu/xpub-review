import React, { Fragment } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { compose, withHandlers, withProps } from 'recompose'

import { Label, Icon, MultiAction, Text, marginHelper } from '../'

const PersonInvitation = ({
  label,
  invitation,
  withUnassigned,
  invitationName,
  removeHEditor,
  invitationStatus,
  resendInvitation,
  revokeInvitation,
  withName = true,
  withResend = true,
}) => (
  <Root>
    {label && <Label>{label}</Label>}
    {withUnassigned && !get(invitation, 'id') && <Text>Unassigned</Text>}
    {withName && <Text ml={label ? 1 : 0}>{invitationName}</Text>}

    {invitationStatus === 'pending' ? (
      <Fragment>
        {withResend && (
          <Modal
            cancelText="BACK"
            component={MultiAction}
            confirmText="RESEND"
            modalKey={`${invitation.id}-resend`}
            onConfirm={resendInvitation}
            subtitle="Are you sure you want to resend the invitation?"
            title={invitationName}
          >
            {showModal => (
              <Icon
                data-test-id="resend-icon"
                fontSize="16px"
                icon="resend"
                mb={1 / 4}
                ml={2}
                onClick={showModal}
              />
            )}
          </Modal>
        )}

        <Modal
          cancelText="BACK"
          component={MultiAction}
          confirmText="REVOKE"
          modalKey={`${invitation.id}-revoke`}
          onConfirm={revokeInvitation}
          subtitle="Are you sure you want to revoke the invitation?"
          title={invitationName}
        >
          {showModal => (
            <Icon
              data-test-id="revoke-icon"
              fontSize="16px"
              icon="remove"
              mb={1 / 4}
              ml={2}
              onClick={showModal}
            />
          )}
        </Modal>
      </Fragment>
    ) : (
      invitationStatus === 'accepted' && (
        <Fragment>
          <Modal
            cancelText="BACK"
            component={MultiAction}
            confirmText="REVOKE"
            modalKey={`${invitation.id}-remove`}
            onConfirm={removeHEditor}
            subtitle="Deleting the handling editor at this moment will also remove all his work."
            title="Revoke invitation?"
          >
            {showModal => (
              <Icon
                data-test-id="revoke-icon"
                fontSize="16px"
                icon="remove"
                mb={1 / 4}
                ml={2}
                onClick={showModal}
              />
            )}
          </Modal>
        </Fragment>
      )
    )}
  </Root>
)

export default compose(
  withProps(({ invitation }) => ({
    invitationName: `${get(invitation, 'alias.name.givenNames', '')} ${get(
      invitation,
      'alias.name.surname',
      '',
    )}`,
    invitationStatus: get(invitation, 'status'),
  })),
  withHandlers({
    revokeInvitation: ({ onRevoke, invitation }) => modalProps => {
      if (typeof onRevoke === 'function') {
        onRevoke(invitation, modalProps)
      }
    },
    resendInvitation: ({ onResend, invitation }) => modalProps => {
      if (typeof onResend === 'function') {
        onResend(invitation, modalProps)
      }
    },
    removeHEditor: ({ onRemove, invitation }) => modalProps => {
      if (typeof onRemove === 'function') {
        onRemove(invitation, modalProps)
      }
    },
  }),
)(PersonInvitation)

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;

  ${marginHelper};
`
// #endregion
