A icon tooltip.

```js
const { Fragment } = require('react');

const TooltipContent = () => <Fragment>
  <Text secondary>
    When an author, editor, or reviewer has a financial/personal interest or
    belief that could affect his/her objectivity, or inappropriately
    influence his/her actions, a potential conflict of interest exists.{' '}
    <ActionLink to="https://www.hindawi.com/editors/coi/">
      More info
    </ActionLink>
  </Text>
</Fragment>;

<IconTooltip primary interactive content={TooltipContent} />
```
