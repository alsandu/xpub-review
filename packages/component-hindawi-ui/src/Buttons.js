import React from 'react'
import { Button } from '@pubsweet/ui'

import Icon from './Icon'

const Buttons = ({ xlarge, large, medium, small, width, icon, children }) => (
  <div>
    {xlarge && (
      <Button size="xLarge">
        <Icon icon="remove" />
        XL Button
      </Button>
    )}
    {large && (
      <Button primary size="large">
        <Icon icon="remove" />
        NORMAL
      </Button>
    )}
    {medium && (
      <Button disabled primary size="medium">
        Medium Button
      </Button>
    )}
    {small && <Button size="small">Small</Button>}

    {false && (
      <Button width={width}>
        <span className={`${icon} fontIconStyle`} />
        Full width
      </Button>
    )}
  </div>
)

export default Buttons
