A radio group with a comment.

```js
const { Formik } = require('formik')
const { Fragment } = require('react')

const Wrapper = () => (
  <Formik>
    {({ handleSubmit, values }) => (
      <Fragment>
        <RadioWithComments
          required
          formValues={values}
          radioFieldName="hasConflicts"
          commentsFieldName="conflictsComments"
          commentsOn="yes"
          commentsLabel="Conflict of interest details"
          radioLabel="Do any authors have conflicts of interest to declare?"
        />
        <RadioWithComments
          formValues={values}
          radioFieldName="hasDataAvailability"
          commentsFieldName="dataAvailabilityComments"
          commentsOn="no"
          commentsLabel="Data availability statement"
          radioLabel="Have you included a data availability statement in your manuscript?"
        />
        <RadioWithComments
          formValues={values}
          radioFieldName="hasFunding"
          commentsFieldName="fundingComments"
          commentsOn="no"
          commentsLabel="Funding statement"
          radioLabel="Have you provided a funding statement in your manuscript?"
        />
      </Fragment>
    )}
  </Formik>
)
;<Wrapper />
```
