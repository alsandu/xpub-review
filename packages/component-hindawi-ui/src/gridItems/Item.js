import PropTypes from 'prop-types'
import styled from 'styled-components'
import { get, isNumber } from 'lodash'

import { marginHelper, paddingHelper, heightHelper } from '../styledHelpers'

/** @component */

const Item = styled.div`
  align-items: ${props => get(props, 'alignItems', 'flex-start')};
  display: ${props => get(props, 'display', 'flex')};
  flex: ${({ flex }) => (isNumber(flex) ? flex : 1)};
  flex-direction: ${props => (props.vertical ? 'column' : 'row')};
  flex-wrap: ${props => get(props, 'flexWrap', 'initial')};
  justify-content: ${props => get(props, 'justify', 'flex-start')};

  ${marginHelper};
  ${paddingHelper};
  ${heightHelper}
`

Item.propTypes = {
  /** Defines how flex items are laid out along the secondary axis. */
  alignItems: PropTypes.string,
  /** How much space should this item take relative to the other items. */
  flex: PropTypes.number,
  /** Sets the flex direction. If true items are layed out in a column. */
  vertical: PropTypes.bool,
  /** Sets whether flex items are forced onto one line or can wrap on multiple ones. */
  flexWrap: PropTypes.string,
  /** Specifies alignment along the main axis. */
  justify: PropTypes.string,
}

export default Item
