import { get } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { marginHelper } from './styledHelpers'

const IconSize = props => {
  const iconSize = css`
    width: calc(${th('gridUnit')} * ${get(props, 'iconSize', 5)});
    height: calc(${th('gridUnit')} * ${get(props, 'iconSize', 5)});
  `
  return css`
    ${iconSize};
  `
}

const Loader = styled.div`
  border-radius: 100%;
  position: relative;
  :before,
  :after {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    border-radius: 100%;
    border: calc(${th('gridUnit')} / 3) solid transparent;
    border-top-color: ${th('colorPrimary')};
  }
  :before {
    z-index: 100;
    animation: spin 1s infinite;
  }
  :after {
    border: calc(${th('gridUnit')} / 3) solid ${th('colorBorder')};
  }
  @keyframes spin {
    0% {
      -webkit-transform: rotate(0deg);
      -ms-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }

  ${IconSize};
  ${marginHelper};
`

export default Loader
