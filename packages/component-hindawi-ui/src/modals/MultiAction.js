import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2, Button, Spinner } from '@pubsweet/ui'
import { compose, setDisplayName, withHandlers } from 'recompose'

import { Icon, Text, Row, withFetching } from '../../'

const MultiAction = ({
  title,
  content,
  onClose,
  subtitle,
  onConfirm,
  isFetching,
  cancelText,
  confirmText,
  fetchingError,
  renderContent,
}) => (
  <Root>
    <Icon
      bold
      color="colorFurnitureHue"
      fontSize="24px"
      icon="remove1"
      onClick={onClose}
      right={5}
      secondary
      top={5}
    />
    <H2>{title}</H2>
    {subtitle && <Text secondary>{subtitle}</Text>}
    {renderContent()}
    {fetchingError && (
      <Text align="center" error>
        {fetchingError}
      </Text>
    )}
    <Row justify={isFetching ? 'center' : 'space-between'} mt={3}>
      {isFetching ? (
        <Spinner size={3} />
      ) : (
        <Fragment>
          <Button data-test-id="modal-cancel" onClick={onClose} width={24}>
            {cancelText}
          </Button>
          <Button
            data-test-id="modal-confirm"
            onClick={onConfirm}
            primary
            width={24}
          >
            {confirmText}
          </Button>
        </Fragment>
      )}
    </Row>
  </Root>
)

MultiAction.propTypes = {
  /** Title that will be showed on the card. */
  title: PropTypes.string,
  /** Subtitle that will be showed on the card. */
  subtitle: PropTypes.string,
  /** The text you want to see on the button when someone submit the report. */
  confirmText: PropTypes.string,
  /** The text you want to see on the button when someone cancel the report. */
  cancelText: PropTypes.string,
  /** Callback function fired when cancel confirmation card. */
  onCancel: PropTypes.func, // eslint-disable-line
  /** Callback function fired when confirm confirmation card. */
  onConfirm: PropTypes.func,
  /** When is true will show a spinner.  */
  onClose: PropTypes.func,
  /** Callback function fired when you want to close the card. */
  isFetching: PropTypes.bool,
  /** The component you want to show on the card. */
  content: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.object,
    PropTypes.string,
  ]),
}
MultiAction.defaultProps = {
  title: undefined,
  subtitle: undefined,
  confirmText: 'OK',
  cancelText: 'Cancel',
  onCancel: undefined,
  onConfirm: undefined,
  onClose: undefined,
  isFetching: undefined,
  content: undefined,
}

export default compose(
  withFetching,
  withHandlers({
    onConfirm: ({ onConfirm, ...props }) => () => {
      if (typeof onConfirm === 'function') {
        onConfirm(props)
      } else {
        props.hideModal()
      }
    },
    onClose: ({ onCancel, ...props }) => () => {
      if (typeof onCancel === 'function') {
        onCancel(props)
      }
      props.hideModal()
    },
    renderContent: ({ content, ...props }) => () => {
      if (!content) return null
      if (typeof content === 'object') {
        return content
      } else if (typeof content === 'function') {
        return content(props)
      }
      return <Text dangerouslySetInnerHTML={{ __html: content }} mb={1} />
    },
  }),
  setDisplayName('MultiActionModal'),
)(MultiAction)

// #region styles
const Root = styled.div`
  align-items: center;
  background: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  position: relative;
  padding: calc(${th('gridUnit')} * 5);
  padding-bottom: calc(${th('gridUnit')} * 3);
  width: calc(${th('gridUnit')} * 65);

  ${H2} {
    margin: 0 0 ${th('gridUnit')} 0;
    text-align: center;
  }

  ${Text} {
    margin-bottom: ${th('gridUnit')};
  }
`
// #endregion
