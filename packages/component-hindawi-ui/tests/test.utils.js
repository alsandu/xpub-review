import React from 'react'
import theme from 'hindawi-theme'
import { ThemeProvider } from 'styled-components'
import { render } from 'react-testing-library'

export const themeWrapper = Component =>
  render(<ThemeProvider theme={theme}>{Component}</ThemeProvider>)
