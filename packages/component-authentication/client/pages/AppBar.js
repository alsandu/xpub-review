import React from 'react'
import { get } from 'lodash'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import { withJournal } from 'xpub-journal'
import { compose, withProps, withHandlers } from 'recompose'

import { queries } from '../graphql'
import { AuthenticatedAppBar, UnauthenticatedAppBar } from '../components'

const unauthedPaths = [
  '/login',
  '/signup',
  '/invite',
  '/eqs-decision',
  '/confirm-signup',
  '/password-reset',
  '/forgot-password',
  '/emails/decline-review',
  '/emails/accept-review-new-user',
]
const AppBar = ({
  goTo,
  logo,
  logout,
  submitButton,
  goToDashboard,
  isUnauthedRoute,
  autosaveIndicator,
}) =>
  isUnauthedRoute ? (
    <UnauthenticatedAppBar goTo={goTo} logo={logo} />
  ) : (
    <Query query={queries.currentUser}>
      {({ data, loading, client }) => {
        const currentUser = get(data, 'currentUser', {})
        return loading ? null : (
          <AuthenticatedAppBar
            autosaveIndicator={autosaveIndicator}
            currentUser={currentUser}
            goTo={goTo}
            goToDashboard={goToDashboard(client)}
            logo={logo}
            logout={logout(client)}
            submitButton={submitButton}
          />
        )
      }}
    </Query>
  )

export default compose(
  withRouter,
  withJournal,
  withProps(({ location, journal }) => ({
    logo: get(journal, 'metadata.logo'),
    isUnauthedRoute: unauthedPaths.includes(location.pathname),
  })),
  withHandlers({
    goToDashboard: ({ history, queries }) => gqlClient => () => {
      const filterInput = JSON.parse(window.localStorage.getItem('filters'))
      gqlClient.query({
        fetchPolicy: 'network-only',
        query: queries.getManuscripts,
        variables: {
          input: {
            priorityFilter: get(filterInput, 'priority', 'all'),
            dateOrder: get(filterInput, 'sort', 'desc'),
          },
        },
      })
      history.push('/')
    },
    goTo: ({ history }) => path => {
      history.push(path)
    },
    logout: ({ history }) => gqlClient => () => {
      gqlClient.resetStore()
      window.localStorage.removeItem('token')
      history.replace('/login')
    },
  }),
)(AppBar)
