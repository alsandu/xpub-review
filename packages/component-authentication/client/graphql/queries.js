import gql from 'graphql-tag'

export const currentUser = gql`
  query {
    currentUser {
      id
      isActive
      role
      identities {
        ... on Local {
          name {
            givenNames
            surname
            title
          }
          isConfirmed
          email
        }
      }
    }
  }
`
