import gql from 'graphql-tag'

export const loginUser = gql`
  mutation localLogin($input: LocalLoginInput!) {
    localLogin(input: $input) {
      token
    }
  }
`

export const signUp = gql`
  mutation signUp($input: SignUpUserInput!) {
    signUp(input: $input) {
      token
    }
  }
`
export const signUpFromInvitation = gql`
  mutation signUpFromInvitation($input: SignUpFromInvitationInput!) {
    signUpFromInvitation(input: $input) {
      token
    }
  }
`

export const confirmUser = gql`
  mutation confirmUser($input: ConfirmUserInput!) {
    confirmAccount(input: $input) {
      token
    }
  }
`

export const requestPasswordReset = gql`
  mutation requestPasswordReset($email: String!) {
    requestPasswordReset(email: $email)
  }
`

export const resetPassword = gql`
  mutation resetPassword($input: ResetPasswordInput!) {
    resetPassword(input: $input)
  }
`
