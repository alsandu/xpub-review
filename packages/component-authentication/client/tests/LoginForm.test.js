import React from 'react'
import 'jest-dom/extend-expect'
import { cleanup, fireEvent } from 'react-testing-library'

import { render } from './testUtils'
import LoginForm from '../components/LoginForm'

describe('LoginForm component', () => {
  afterEach(cleanup)

  it('should login', async done => {
    const loginUserMock = jest.fn()
    const { getByTestId, getByText } = render(
      <LoginForm loginUser={loginUserMock} />,
    )

    fireEvent.change(getByTestId(/login-email/i), {
      target: { value: 'testemail@gmail.com' },
    })

    fireEvent.change(getByTestId(/login-password/i), {
      target: { value: 'astrongpassword' },
    })

    fireEvent.click(getByText('LOG IN'))

    setTimeout(() => {
      expect(loginUserMock).toHaveBeenCalledTimes(1)
      expect(loginUserMock).toHaveBeenCalledWith(
        {
          email: 'testemail@gmail.com',
          password: 'astrongpassword',
        },
        expect.anything(),
      )
      done()
    })
  })

  it('should validate missing form fields', async done => {
    const loginUserMock = jest.fn()
    const { getByText } = render(<LoginForm loginUser={loginUserMock} />)

    fireEvent.click(getByText('LOG IN'))
    setTimeout(() => {
      expect(getByText('Required')).toBeInTheDocument()

      done()
    })
  })
})
