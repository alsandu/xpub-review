import React from 'react'
import 'jest-dom/extend-expect'
import { cleanup, fireEvent } from 'react-testing-library'

import { render } from './testUtils'
import { AuthenticatedAppBar } from '../components'

const confirmedUser = {
  isActive: true,
  role: 'admin',
  identities: [
    {
      name: {
        givenNames: 'Aurel',
        surname: 'Becali',
        title: 'mrs',
      },
      isConfirmed: true,
      email: 'alexandru.munteanu@thinslices.com',
    },
  ],
}

const handlingEditor = {
  isActive: true,
  role: 'handlingEditor',
  identities: [
    {
      name: {
        givenNames: 'Aurel',
        surname: 'Becali',
        title: 'mrs',
      },
      isConfirmed: true,
      email: 'alexandru.munteanu@thinslices.com',
    },
  ],
}

const unconfirmedUser = {
  isActive: true,
  role: 'admin',
  identities: [
    {
      name: {
        givenNames: 'Aurel',
        surname: 'Becali',
        title: 'mrs',
      },
      isConfirmed: false,
      email: 'alexandru.munteanu@thinslices.com',
    },
  ],
}

describe('AuthenticatedAppBar', () => {
  afterEach(cleanup)

  it('should show username and submit button', () => {
    const { getByText, queryByText } = render(
      <AuthenticatedAppBar
        autosaveIndicator={() => <span>autosave</span>}
        currentUser={confirmedUser}
        submitButton={() => <span>submit</span>}
      />,
    )

    expect(getByText(/Aurel/i)).toBeInTheDocument()
    expect(getByText(/submit/i)).toBeInTheDocument()
    expect(queryByText(/autosave/i)).toBeNull()
  })

  it('should not show submit button while on wizard route', () => {
    const cfg = {
      // set the router on the submission wizard route
      initialEntries: ['/submit/1234567-12345/5512321-12311'],
    }

    const { getByText, queryByText } = render(
      <AuthenticatedAppBar
        autosaveIndicator={() => <span>autosave</span>}
        currentUser={confirmedUser}
        submitButton={() => <span>submit</span>}
      />,
      cfg,
    )

    expect(getByText(/Aurel/i)).toBeInTheDocument()
    expect(queryByText(/submit/i)).toBeNull()
    expect(getByText(/autosave/i)).toBeInTheDocument()
  })

  it('should show notification for unconfirmed users', () => {
    const { getByText } = render(
      <AuthenticatedAppBar
        autosaveIndicator={() => <span>autosave</span>}
        currentUser={unconfirmedUser}
        submitButton={() => <span>submit</span>}
      />,
    )

    expect(
      getByText('Your account is not confirmed. Please check your email.'),
    ).toBeInTheDocument()
  })

  it('should be able to click logo and go to dashboard', () => {
    const goToDashboardMock = jest.fn()
    const goToMock = jest.fn(() => goToDashboardMock)
    const { container } = render(
      <AuthenticatedAppBar
        autosaveIndicator={() => <span>autosave</span>}
        currentUser={confirmedUser}
        goToDashboard={goToMock()}
        submitButton={() => <span>submit</span>}
      />,
    )

    fireEvent.click(container.querySelector('img'))

    expect(goToDashboardMock).toHaveBeenCalledTimes(1)
  })

  it('should interact with the menu', () => {
    const logoutMock = jest.fn()
    const goToMock = jest.fn()

    const { getByText } = render(
      <AuthenticatedAppBar
        autosaveIndicator={() => <span>autosave</span>}
        currentUser={handlingEditor}
        goTo={goToMock}
        logout={logoutMock}
        submitButton={() => <span>submit</span>}
      />,
    )

    fireEvent.click(getByText(/aurel/i))
    fireEvent.click(getByText(/my profile/i))

    expect(goToMock).toHaveBeenCalledTimes(1)
    expect(goToMock).toHaveBeenCalledWith('/profile')

    fireEvent.click(getByText(/aurel/i))
    fireEvent.click(getByText(/logout/i))

    expect(logoutMock).toHaveBeenCalledTimes(1)
  })
})
