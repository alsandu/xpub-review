Sign up form.

```js
const { Formik } = require('formik')
const { withSteps } = require('component-hindawi-ui')

const journal = {
  titles: [{ value: 'mr', label: 'Mr' }, { value: 'ms', label: 'Ms' }],
}

const SignUp = withSteps(stepProps => (
  <SignUpForm
    onSubmit={stepProps.step === 0 ? stepProps.nextStep : null}
    journal={journal}
    {...stepProps}
  />
))

;<SignUp />
```

Sign up form with pending request.

```js
const { Formik } = require('formik')
const { withSteps } = require('component-hindawi-ui')

const journal = {
  titles: [{ value: 'mr', label: 'Mr' }, { value: 'ms', label: 'Ms' }],
}

const SignUp = withSteps(stepProps => (
  <Formik onSubmit={stepProps.step === 0 ? stepProps.nextStep : null}>
    {({ values, errors, touched, handleSubmit }) => (
      <SignUpForm
        isFetching
        journal={journal}
        values={values}
        errors={errors}
        touched={touched}
        handleSubmit={handleSubmit}
        {...stepProps}
      />
    )}
  </Formik>
))

;<SignUp />
```

Sign up form with request error.

```js
const { Formik } = require('formik')
const { withSteps } = require('component-hindawi-ui')

const journal = {
  titles: [{ value: 'mr', label: 'Mr' }, { value: 'ms', label: 'Ms' }],
}

const SignUp = withSteps(stepProps => (
  <Formik onSubmit={stepProps.step === 0 ? stepProps.nextStep : null}>
    {({ values, errors, touched, handleSubmit }) => (
      <SignUpForm
        fetchingError="Oops! Something went wrong..."
        journal={journal}
        values={values}
        errors={errors}
        touched={touched}
        handleSubmit={handleSubmit}
        {...stepProps}
      />
    )}
  </Formik>
))

;<SignUp />
```
