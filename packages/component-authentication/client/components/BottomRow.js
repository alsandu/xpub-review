import styled from 'styled-components'
import { Row } from 'component-hindawi-ui'
import { th } from '@pubsweet/ui-toolkit'

export default styled(Row)`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom-left-radius: ${th('gridUnit')};
  border-bottom-right-radius: ${th('gridUnit')};
`
