import React from 'react'
import { Logo } from 'component-hindawi-ui'

import { LogoContainer, Root } from './sharedStyledComponents'

const UnauthenticatedAppBar = ({ logo, goTo }) => (
  <Root>
    <LogoContainer>
      <Logo goTo={() => goTo('/')} height={54} src={logo} title="Hindawi" />
    </LogoContainer>
  </Root>
)

export default UnauthenticatedAppBar
