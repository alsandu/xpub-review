process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { cloneDeep } = require('lodash')
const { fixtures, models, services } = require('fixture-service')

const { signUpUseCase } = require('../src/use-cases')

const chance = new Chance()

const notificationService = {
  notifyUserOnSignUp: jest.fn(),
}

const basicInput = {
  firstName: chance.first(),
  lastName: chance.first(),
  title: 'Dr.',
  country: chance.country(),
  affiliation: chance.company(),
  password: 'Password1!',
  email: chance.email(),
  agreeTc: true,
}

describe('signup use case', () => {
  let input

  beforeEach(() => {
    input = cloneDeep(basicInput)
  })

  it('should return a new user', async () => {
    const mockedModels = models.build(fixtures)

    const result = await signUpUseCase
      .initialize(notificationService, services.tokenService, mockedModels)
      .execute(input)

    expect(result.token).toBeDefined()
  })

  it('should return an error when the user is logged in', async () => {
    const mockedModels = models.build(fixtures)
    try {
      await signUpUseCase
        .initialize(notificationService, services.tokenService, mockedModels)
        .execute(input, chance.hash())
    } catch (e) {
      expect(e.message).toEqual('Cannot sign up while logged in.')
    }
  })

  it('should return an error when agree T&C is false', async () => {
    const mockedModels = models.build(fixtures)

    input.agreeTC = false
    try {
      await signUpUseCase
        .initialize(notificationService, services.tokenService, mockedModels)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('Terms & Conditions must be read and approved.')
    }
  })

  it('should return an error when the password is weak', async () => {
    const mockedModels = models.build(fixtures)

    input.password = 'weak-password'
    try {
      await signUpUseCase
        .initialize(notificationService, services.tokenService, mockedModels)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual(
        'Password is too weak. Please check password requirements.',
      )
    }
  })
})
