process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const { models, fixtures, services } = require('fixture-service')

const confirmAccountUseCase = require('../src/use-cases/confirmAccount')

const chance = new Chance()

describe('confirm use case', () => {
  it('should return success when the user is not confirmed and the token is correct', async () => {
    const user = fixtures.generateUser({ isConfirmed: false })
    const mockedModels = models.build(fixtures)

    await confirmAccountUseCase
      .initialize({ tokenService: services.tokenService, models: mockedModels })
      .execute({
        userId: user.id,
        token: user.confirmationToken,
      })

    expect(user.identities[0].isConfirmed).toBeTruthy()
    expect(user.confirmationToken).toBeNull()
  })

  it('should return an error when the user is confirmed', async () => {
    const user = fixtures.generateUser({})
    const mockedModels = models.build(fixtures)

    try {
      await confirmAccountUseCase
        .initialize({
          tokenService: services.tokenService,
          models: mockedModels,
        })
        .execute({ userId: user.id, token: user.confirmationToken }, {})
    } catch (e) {
      expect(e.message).toEqual('User is already confirmed.')
    }
  })

  it('should return an error when the token is invalid', async () => {
    const user = fixtures.generateUser({ isConfirmed: false })
    const mockedModels = models.build(fixtures)

    try {
      await confirmAccountUseCase
        .initialize({
          tokenService: services.tokenService,
          models: mockedModels,
        })
        .execute({ userId: user.id, token: chance.guid() }, {})
    } catch (e) {
      expect(e.message).toEqual('Invalid confirmation token.')
    }
  })
})
