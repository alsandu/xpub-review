const config = require('config')
const Email = require('@pubsweet/component-email-templating')

const { services } = require('helper-service')
const { getEmailCopy } = require('./emailCopy')

const { name: journalName, staffEmail } = config.get('journal')
const unsubscribeSlug = config.get('unsubscribe.url')
const baseUrl = config.get('pubsweet-client.baseUrl')

module.exports = {
  async notifyUserOnSignUp({ user, identity }) {
    const confirmSignUp = config.get('confirm-signup.url')

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'user-signup',
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: identity.email,
        name: `${identity.surname}`,
      },
      content: {
        subject: 'Confirm your email address',
        ctaLink: services.createUrl(baseUrl, confirmSignUp, {
          userId: user.id,
          confirmationToken: user.confirmationToken,
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        signatureName: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async sendForgotPasswordEmail({ user, identity }) {
    const forgotPath = config.get('forgot-password.url')

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'user-forgot-password',
    })

    const email = new Email({
      type: 'system',
      toUser: {
        email: identity.email,
      },
      fromEmail: `${journalName} <${staffEmail}>`,
      content: {
        subject: 'Forgot Password',
        ctaLink: services.createUrl(baseUrl, forgotPath, {
          email: identity.email,
          token: user.passwordResetToken,
        }),
        ctaText: 'RESET PASSWORD',
        paragraph,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
