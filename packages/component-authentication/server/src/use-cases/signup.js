const { passwordStrengthRegex } = require('config')
const uuid = require('uuid')

const initialize = (notificationService, tokenService, { User, Identity }) => ({
  execute: async input => {
    if (!input.agreeTc) {
      throw new Error('Terms & Conditions must be read and approved.')
    }

    if (!passwordStrengthRegex.test(input.password)) {
      throw new Error(
        'Password is too weak. Please check password requirements.',
      )
    }

    const user = new User({
      defaultIdentity: 'local',
      isActive: true,
      agreeTc: input.agreeTc,
      confirmationToken: uuid.v4(),
    })

    const passwordHash = await Identity.hashPassword(input.password)

    const identity = new Identity({
      type: 'local',
      isConfirmed: false,
      passwordHash,
      email: input.email,
      aff: input.aff,
      country: input.country,
      surname: input.surname,
      givenNames: input.givenNames,
      title: input.title,
    })

    user.assignIdentity(identity)

    await user.saveRecursively()

    const token = tokenService.create({
      username: identity.email,
      id: user.id,
    })

    await notificationService.notifyUserOnSignUp({ user, identity })

    return { token }
  },
})

const authsomePolicies = ['unauthenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
