const initialize = ({ loggedUser, models }) => ({
  execute: async ({ user }) => {
    const currentUser = await loggedUser.getCurrentUser({ models, user })
    currentUser.identities = [currentUser.getDefaultIdentity()]
    return currentUser.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
