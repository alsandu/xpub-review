const { passwordStrengthRegex } = require('config')

const initialize = (tokenService, { Identity }) => ({
  execute: async input => {
    if (!input.agreeTc) {
      throw new Error('Terms & Conditions must be read and approved.')
    }

    if (!passwordStrengthRegex.test(input.password)) {
      throw new Error(
        'Password is too weak. Please check password requirements.',
      )
    }

    const identity = await Identity.findOneByField('email', input.email, 'user')
    if (!identity || !identity.user.isActive) {
      throw new Error('Something went wrong.')
    }

    if (identity.user.confirmationToken !== input.confirmationToken) {
      throw new Error('Invalid confirmation token.')
    }

    const passwordHash = await Identity.hashPassword(input.password)

    identity.updateProperties({
      givenNames: input.givenNames,
      surname: input.surname,
      title: input.title,
      country: input.country,
      aff: input.aff,
      passwordHash,
      isConfirmed: true,
    })

    identity.user.confirmationToken = null

    await identity.saveRecursively()

    const token = tokenService.create({
      username: identity.email,
      id: identity.user.id,
    })

    return { token }
  },
})

const authsomePolicies = ['unauthenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
