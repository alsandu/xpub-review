const initialize = (tokenService, { Identity }) => ({
  execute: async ({ email, password }) => {
    const identity = await Identity.findOneByField('email', email, 'user')

    if (!identity) {
      throw new Error('Wrong username or password.')
    }

    const { user } = identity
    if (!(await identity.validPassword(password)) || !user.isActive) {
      throw new Error('Wrong username or password.')
    }

    const token = tokenService.create({
      username: identity.email,
      id: identity.userId,
    })

    return { token }
  },
})

const authsomePolicies = ['unauthenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
