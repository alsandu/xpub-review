const initialize = ({ User }) => ({
  execute: async ({ currentUser, input }) => {
    const user = await User.find(currentUser, 'identities')
    const localIdentity = user.getDefaultIdentity()
    localIdentity.updateProperties(input)
    await localIdentity.save()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
