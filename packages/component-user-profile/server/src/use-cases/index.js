const updateUserUseCase = require('./updateUser')
const subscribeToEmailsUseCase = require('./subscribeToEmails')
const unsubscribeToEmailsUseCase = require('./unsubscribeToEmails')
const changePasswordUseCase = require('./changePassword')
const unlinkOrcidUseCase = require('./unlinkOrcid')

module.exports = {
  updateUserUseCase,
  unlinkOrcidUseCase,
  changePasswordUseCase,
  subscribeToEmailsUseCase,
  unsubscribeToEmailsUseCase,
}
