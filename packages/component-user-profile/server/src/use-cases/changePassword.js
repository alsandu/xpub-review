const config = require('config')

const initialize = (tokenService, { User, Identity }) => ({
  execute: async ({ oldPassword, password }, currentUser) => {
    if (!config.passwordStrengthRegex.test(password))
      throw new Error(
        'Password is too weak. Please check password requirements.',
      )

    const user = await User.find(currentUser, 'identities')

    const localIdentity = user.identities.find(i => i.type === 'local')

    if (localIdentity && !(await localIdentity.validPassword(oldPassword))) {
      throw new Error('Wrong username or password.')
    }

    const passwordHash = await Identity.hashPassword(password)

    localIdentity.updateProperties({ passwordHash })
    await localIdentity.save()

    const token = tokenService.create({
      username: localIdentity.email,
      id: user.id,
    })

    return { token }
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
