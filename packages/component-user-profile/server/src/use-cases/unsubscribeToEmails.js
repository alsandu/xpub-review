const initialize = ({ User }) => ({
  execute: async ({ currentUser, input }) => {
    const user = await User.find(currentUser)
    if (!user.isSubscribedToEmails) return
    if (user.unsubscribeToken !== input.token) {
      throw new Error(`Invalid token ${input.token}.`)
    }

    user.updateProperties({ isSubscribedToEmails: false })
    await user.save()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
