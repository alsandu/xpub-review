import React from 'react'
import { Row, Item, Label, ActionLink, ShadowedBox } from 'component-hindawi-ui'

const EmailNotifications = ({ isSubscribed, toggeleEmailSubscription }) => (
  <ShadowedBox mt={2}>
    <Row alignItems="center">
      <Item>
        <Label>Email Notifications </Label>
      </Item>
      <Item justify="flex-end">
        <ActionLink fontWeight={700} onClick={toggeleEmailSubscription}>
          {isSubscribed ? 'Unsubscribe' : 'Re-subscribe'}
        </ActionLink>
      </Item>
    </Row>
  </ShadowedBox>
)

export default EmailNotifications
