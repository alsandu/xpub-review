import gql from 'graphql-tag'
import { userDetailsWithIdentities } from './fragments'

export const getCurrentUser = gql`
  query {
    getCurrentUser {
      ...userDetailsWithIdentities
      isSubscribedToEmails
      unsubscribeToken
    }
  }
  ${userDetailsWithIdentities}
`
