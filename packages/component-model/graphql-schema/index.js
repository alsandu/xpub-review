const fs = require('fs')
const path = require('path')

const sharedTypeDefs = fs.readFileSync(
  path.join(__dirname, 'shared-data-model.graphqls'),
  'utf8',
)
const hindawiTypeDefs = fs.readFileSync(
  path.join(__dirname, 'hindawi-data-model.graphqls'),
  'utf8',
)

module.exports = `
${sharedTypeDefs}
${hindawiTypeDefs}
`
