/* eslint react/prefer-stateless-function: 0 */
import React, { Component, Fragment } from 'react'
import hindawiTheme from 'hindawi-theme'
import { th } from '@pubsweet/ui-toolkit'
import { ModalProvider } from 'component-modal'
import styled, { ThemeProvider } from 'styled-components'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { BrowserRouter as Router } from 'react-router-dom'

import withDragAndDrop from './withDragAndDrop'

const StyleRoot = styled.div`
  color: ${th('colorText')};
  font-family: ${th('defaultFont')}, sans-serif;
  font-size: ${th('fontSizeBase')};

  * {
    box-sizing: border-box;
  }
`
const client = new ApolloClient({
  link: new HttpLink(),
  cache: new InMemoryCache(),
})

class Wrapper extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Fragment>
          <div id="ps-modal-root" />
          <Router>
            <ModalProvider>
              <ThemeProvider theme={hindawiTheme}>
                <StyleRoot>{this.props.children}</StyleRoot>
              </ThemeProvider>
            </ModalProvider>
          </Router>
        </Fragment>
      </ApolloProvider>
    )
  }
}

export default withDragAndDrop(Wrapper)
