const MODAL_ROOT_ID = 'ps-modal-root'

export default () => {
  const rootElement = document.getElementById(MODAL_ROOT_ID)
  if (rootElement) {
    return rootElement
  }

  const modalDiv = document.createElement('div')
  modalDiv.setAttribute('id', MODAL_ROOT_ID)
  document.body.insertAdjacentElement('beforeend', modalDiv)

  return modalDiv
}
