import { useEffect, useContext } from 'react'
import { ModalContext } from '..'

const useModal = ({ modalKey, component }) => {
  const { registerModal, unregisterModal, ...modalContext } = useContext(
    ModalContext,
  )

  useEffect(() => {
    registerModal({ modalKey, component })
    return () => unregisterModal(modalKey)
  }, [])

  return {
    ...modalContext,
    showModal: props => () => {
      modalContext.showModal(modalKey, props)
    },
  }
}

export default useModal
