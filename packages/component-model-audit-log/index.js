const AuditLog = require('./src/auditLog')

module.exports = {
  model: AuditLog,
  modelName: 'AuditLog',
}
