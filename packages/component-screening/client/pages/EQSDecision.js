import React from 'react'

import { H2, Button } from '@pubsweet/ui'
import { Text, Row, ShadowedBox } from 'component-hindawi-ui'
import { Modal } from 'component-modal'
import { compose, withHandlers, withProps } from 'recompose'

import { DeclineEQSModal, AcceptEQSModal } from '../components'
import withGQL from '../graphql'
import { parseSearchParams } from '../utils'

const EQSDecision = ({ handleDeclineEQS, handleApproveEQS, title }) => (
  <ShadowedBox center mt={3} width={65}>
    <H2 mt={3}>Editorial decision</H2>

    <Row justify="center" mt={1}>
      <Text secondary>
        Did manuscript titled{' '}
        <Text display="inline" fontWeight="bold" secondary>
          {title}
        </Text>{' '}
        pass EQS checks?
      </Text>
    </Row>

    <Row justify="space-around" mb={3} mt={3}>
      <Modal
        component={DeclineEQSModal}
        handleDeclineEQS={handleDeclineEQS}
        modalKey="rejectEQS"
      >
        {showModal => (
          <Button
            data-test-id="reject-button"
            mr={1}
            onClick={showModal}
            secondary
            width={24}
          >
            NO
          </Button>
        )}
      </Modal>
      <Modal
        component={AcceptEQSModal}
        handleApproveEQS={handleApproveEQS}
        modalKey="acceptEQS"
      >
        {showModal => (
          <Button
            data-test-id="accept-button"
            onClick={showModal}
            primary
            width={24}
          >
            YES
          </Button>
        )}
      </Modal>
    </Row>
  </ShadowedBox>
)

export default compose(
  withGQL,
  withProps(({ history }) => {
    const { title, token, manuscriptId } = parseSearchParams(
      history.location.search,
    )

    return { title, token, manuscriptId }
  }),
  withHandlers({
    handleDeclineEQS: ({ declineEQS, token, manuscriptId, history }) => ({
      setFetching,
      setError,
      hideModal,
    }) => {
      setFetching(true)

      declineEQS({
        variables: {
          manuscriptId,
          input: {
            token,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          history.push('/info-page', {
            content: 'Manuscript rejected. Thank you for your technical check!',
            title: 'Editorial decision',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    handleApproveEQS: ({ approveEQS, token, manuscriptId, history }) => (
      { customId },
      { setFetching, setError, hideModal },
    ) => {
      setFetching(true)

      approveEQS({
        variables: {
          manuscriptId,
          input: {
            token,
            customId,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          history.push('/info-page', {
            content: 'Manuscript accepted. Thank you for your technical check!',
            title: 'Editorial decision',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(EQSDecision)
