/* eslint-disable */
import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { queries } from 'component-activity-log/client'

import * as mutations from './mutations'
import { parseSearchParams } from '../utils'

const mutationOptions = ({ location }) => {
  const { manuscriptId } = parseSearchParams(location.search)
  return {
    refetchQueries: [
      {
        query: queries.getAuditLogEvents,
        variables: { manuscriptId },
      },
      'getManuscripts',
    ],
  }
}

export default compose(
  graphql(mutations.declineEQS, {
    name: 'declineEQS',
    options: mutationOptions,
  }),
  graphql(mutations.approveEQS, {
    name: 'approveEQS',
    options: mutationOptions,
  }),
  graphql(mutations.approveEQA, {
    name: 'approveEQA',
    options: mutationOptions,
  }),
  graphql(mutations.returnToEiC, {
    name: 'returnToEiC',
    options: mutationOptions,
  }),
)
