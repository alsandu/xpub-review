import React from 'react'

import {
  Row,
  Item,
  Label,
  FormModal,
  Textarea,
  validators,
  ValidatedFormField,
} from 'component-hindawi-ui'

const ReturnManuscriptToEiCModal = ({ handleReturnToEiC, hideModal }) => (
  <FormModal
    cancelText="CANCEL"
    confirmText="OK"
    content={() => (
      <Row mt={3}>
        <Item vertical>
          <Label required>Return reason</Label>
          <ValidatedFormField
            component={Textarea}
            data-test-id="return-reason"
            name="reason"
            validate={[validators.required]}
          />
        </Item>
      </Row>
    )}
    hideModal={hideModal}
    onSubmit={handleReturnToEiC}
    title="Reject manuscript"
  />
)

export default ReturnManuscriptToEiCModal
