export const parseSearchParams = url => {
  const params = new URLSearchParams(url)
  const parsedObject = {}
  /* eslint-disable */
  for ([key, value] of params) {
    parsedObject[key] = value
  }
  /* eslint-enable */
  return parsedObject
}
