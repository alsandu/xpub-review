const initialize = ({ models: { Manuscript }, logEvent }) => ({
  async execute({ manuscriptId, input, reqUserId }) {
    const { customId, token } = input
    const manuscript = await Manuscript.find(manuscriptId)
    const duplicateId = await Manuscript.findOneByField('customId', customId)

    if (manuscript.hasPassedEqs !== null) {
      throw new Error('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    if (duplicateId) {
      throw new Error('The provided ID already exists.')
    }

    manuscript.updateProperties({
      customId,
      hasPassedEqs: true,
      technicalCheckToken: null,
      status: 'submitted',
    })
    await manuscript.save()

    logEvent({
      userId: null,
      manuscriptId,
      action: logEvent.actions.eqs_approved,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
