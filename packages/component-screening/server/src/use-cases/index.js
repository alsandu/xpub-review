const approveEQSUseCase = require('./approveEQS')
const declineEQSUseCase = require('./declineEQS')
const approveEQAUseCase = require('./approveEQA')
const returnToEiCUseCase = require('./returnToEiC')

module.exports = {
  approveEQSUseCase,
  declineEQSUseCase,
  approveEQAUseCase,
  returnToEiCUseCase,
}
