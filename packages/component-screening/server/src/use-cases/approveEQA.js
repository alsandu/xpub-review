const initialize = ({ notification, models: { Manuscript } }) => ({
  async execute({ manuscriptId, input }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.user, journal.teams.members]',
    )

    if (manuscript.status !== Manuscript.Statuses.inQA) {
      throw new Error(`Cannot approve EQA in the current status.`)
    }

    const { token } = input
    if (manuscript.hasPassedEqa !== null) {
      throw new Error('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    manuscript.updateProperties({
      technicalCheckToken: null,
      hasPassedEqa: true,
      status: Manuscript.Statuses.accepted,
    })
    await manuscript.save()

    const { journal } = manuscript
    const editorInChief = journal.getEditorInChief()
    const handlingEditor = manuscript.getHandlingEditor()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const authors = manuscript.getAuthors()
    const reviewers = manuscript.getReviewersByStatus('submitted')

    notification.notifyHEWhenEQAApprovesManuscript({
      submittingAuthor,
      editorInChief,
      handlingEditor,
      manuscript,
    })
    notification.notifyAuthorsWhenEQAApprovesManuscript({
      authors,
      editorInChief,
      manuscript,
    })
    notification.notifyReviewersWhenEQAApprovesManuscript({
      reviewers,
      submittingAuthor,
      editorInChief,
      manuscript,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
