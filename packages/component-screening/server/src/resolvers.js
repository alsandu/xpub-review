const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')
const notifications = require('./notifications/notifications')

const useCases = require('./use-cases')

const resolvers = {
  Query: {},
  Mutation: {
    async approveEQS(_, { manuscriptId, input }, ctx) {
      return useCases.approveEQSUseCase
        .initialize({ models, logEvent })
        .execute({ manuscriptId, input, reqUserId: ctx.user })
    },
    async declineEQS(_, { manuscriptId, input }, ctx) {
      return useCases.declineEQSUseCase
        .initialize({ models, logEvent })
        .execute({ manuscriptId, input, reqUserId: ctx.user })
    },
    async approveEQA(_, { manuscriptId, input }, ctx) {
      return useCases.approveEQAUseCase
        .initialize({ models, notification: notifications })
        .execute({ manuscriptId, input })
    },
    async returnToEiC(_, { manuscriptId, input }, ctx) {
      return useCases.returnToEiCUseCase
        .initialize({ notification: notifications, models })
        .execute({ manuscriptId, input })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
