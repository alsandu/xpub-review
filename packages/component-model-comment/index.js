const Comment = require('./src/comment')

module.exports = {
  model: Comment,
  modelName: 'Comment',
}
