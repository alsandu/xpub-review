const { HindawiBaseModel } = require('component-model')

class Comment extends HindawiBaseModel {
  static get tableName() {
    return 'comment'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        type: { enum: Object.values(Comment.Types) },
        content: { type: ['string', 'null'] },
        reviewId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      review: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-review').model,
        join: {
          from: 'comment.reviewId',
          to: 'review.id',
        },
      },
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-file').model,
        join: {
          from: 'comment.id',
          to: 'file.commentId',
        },
      },
    }
  }

  static get Types() {
    return {
      public: 'public',
      private: 'private',
    }
  }

  assignFile(file) {
    this.files = this.files || []
    this.files.push(file)
  }

  toDTO() {
    return { ...this, files: this.files ? this.files.map(f => f.toDTO()) : [] }
  }
}

module.exports = Comment
